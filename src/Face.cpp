/*
 * Face.cpp
 *
 *  Created on: 23/04/2014
 *      Author: leobrizolara
 */

#include "Face.h"

using namespace cv;
using namespace std;

const int Face::unknownPerson = -1;

Face::Face(const cv::Rect & faceRegion)
: personId(unknownPerson)
, region(Rect(faceRegion))
, leftEye(Rect(0,0,0,0))
, rightEye(Rect(0,0,0,0))
{
}

int Face::getPersonId() const {
	return personId;
}

void Face::setPersonId(int peopleId) {
	this->personId = peopleId;
}

const cv::Rect& Face::getRegion() const {
	return region;
}

const cv::Rect& Face::getLeftEye() const {
	return leftEye;
}

void Face::setLeftEye(const cv::Rect& leftEye) {
	this->leftEye = leftEye;
}

const cv::Rect& Face::getRightEye() const {
	return rightEye;
}

void Face::setRightEye(const cv::Rect& rightEye) {
	this->rightEye = rightEye;
}

void Face::setRegion(const cv::Rect& region) {
	this->region = region;
}

bool Face::hasRightEye() const {
	return this->rightEye.area() > 0;
}

std::vector<cv::Rect> Face::getEyes() const {
	vector<Rect> eyes;
	if(this->hasLeftEye()){
		eyes.push_back(this->leftEye);
	}
	if(this->hasRightEye()){
		eyes.push_back(this->rightEye);
	}
	return eyes;
}

bool Face::hasLeftEye() const {
	return this->leftEye.area() > 0;
}
