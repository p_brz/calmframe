/*
 * App.cpp
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#include "App.h"

#include <iostream>
#include <opencv2/highgui.hpp> //waitKey

using namespace std;

const int App::KEY_ESC = 27;
const char * App::windowName = "window";

App::App()
	: delay(1)
{}

int App::run(int argc, char * argv[]) {
	if(!this->setup(argc,argv)){
		return 1;
	}
	else{
		while(update()){
			char key = (char)cv::waitKey(delay);
			if(key == KEY_ESC){
				break;
			}
			else if (key != -1){
				keyPressed(key);
			}
		}
		return 0;
	}
}

