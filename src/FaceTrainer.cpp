/*
 * FaceTrainer.cpp
 *
 *  Created on: 18/05/2014
 *      Author: leobrizolara
 */

#include <FaceTrainer.h>

void FaceTrainer::addFaceToTrain(const cv::Mat& img, const Face& face,
		int personId) {
}

void FaceTrainer::saveTrainedFaces(const std::string& directory) const {
}

int FaceTrainer::getFacesToTrain(std::vector<cv::Mat>& preparedFaces,
		std::vector<int>& personIds) const
{
	return 0;
}

void FaceTrainer::train(FaceIdentifier& faceIdentifier) {
}
