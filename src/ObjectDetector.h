/*
 * ObjectDetector.h
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#ifndef OBJECTDETECTOR_H_
#define OBJECTDETECTOR_H_

#include <vector>
#include <string>
#include <opencv2/objdetect.hpp>
#include <opencv2/core.hpp>

class ObjectDetector {
private:
	bool ready;
	cv::Ptr<cv::CascadeClassifier> cascadeClassifier;

	int detectionFlags;
	cv::Size detectionMinSize;
	cv::Size detectionMaxSize;
	double detectionScaleFactor;
	int detectionMinNeighbors;
public:
	ObjectDetector(const std::string & cascadeFilename = ""
            , int detectionFlags = cv::CASCADE_SCALE_IMAGE
            , cv::Size detectionMinSize = cv::Size()
            , cv::Size detectionMaxSize = cv::Size()
			, double detectionScaleFactor = 1.1
			, int detectionMinNeighbors = 3);
	bool setup(const std::string & cascadeFilename);
	bool isReady() const;
	int detectObjects(const cv::_InputArray& image, std::vector<cv::Rect> & objects
			, cv::Mat* processedImage = NULL) const;
	int detectObjects(const cv::_InputArray& image, std::vector<cv::Mat> & objImgs
			, bool grayscaleImgs = false, bool copyImages = false) const;
	int detectLargestObject(const cv::_InputArray& image, cv::Rect & largestObject
			, cv::Mat* processedImage = NULL) const;
	int detectLargestObject(const cv::_InputArray& image, cv::Mat & objImg
			, bool grayscaleImgs = false, bool copyImages = false) const;

	//getters
	int getDetectionFlags()               const {return detectionFlags;}
	int getDetectionMinNeighbors() 		  const {return detectionMinNeighbors;}
	const cv::Size& getDetectionMinSize() const {return detectionMinSize;}
	const cv::Size& getDetectionMaxSize() const {return detectionMaxSize;}
	double getDetectionScaleFactor()      const {return detectionScaleFactor;}

	//setters
	void setDetectionFlags(int detectionFlags) {
		this->detectionFlags = detectionFlags;
	}
	void setDetectionMinNeighbors(int detectionMinNeighbors) {
		this->detectionMinNeighbors = detectionMinNeighbors;
	}
	void setDetectionMinSize(const cv::Size& detectionMinSize) {
		this->detectionMinSize = detectionMinSize;
	}
	void setDetectionMaxSize(const cv::Size& detectionMaxSize) {
		this->detectionMaxSize = detectionMaxSize;
	}
	void setDetectionScaleFactor(double detectionScaleFactor) {
		this->detectionScaleFactor = detectionScaleFactor;
	}

protected:
	int detectObjectsImpl(const cv::_InputArray& image, std::vector<cv::Rect> & objects
			, cv::Mat * processedImage = NULL) const;
	int detectObjectsImpl(const cv::_InputArray& image, std::vector<cv::Rect> & objects
			, int flags, cv::Mat * processedImage=NULL) const;
	cv::Mat prepareImage(const cv::_InputArray& image) const;

	void correctBorderObjects(const cv::_InputArray& img, std::vector<cv::Rect> & tempObjects) const;
};

#endif /* OBJECTDETECTOR_H_ */
