/*
 * FaceDetector.cpp
 *
 *  Created on: 15/04/2014
 *      Author: leobrizolara
 */

#include "FaceDetector.h"

#include <assert.h>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdexcept>

#include <iostream>

using namespace cv;
using namespace std;



FaceDetector::FaceDetector(const std::string& cascadeFilename, const std::string & eyesCascadeFilename)
	: faceDetector(cascadeFilename)
	, eyesDetector(eyesCascadeFilename)
{
	this->faceDetector.setDetectionMinSize(Size(20,20));
}

bool FaceDetector::setup(const std::string& faceCascadeFilename)
{
	faceDetector.setup(faceCascadeFilename);
	return this->isReady();
}

bool FaceDetector::isReady() const {
	return faceDetector.isReady();
}


bool FaceDetector::setupEyesDetector(const std::string& eyesCascadeFilename) {
	eyesDetector.setup(eyesCascadeFilename);

	return eyesDetector.isReady();
}

bool FaceDetector::eyesDetectorIsReady() const {
	return eyesDetector.isReady();
}

EyeDetector& FaceDetector::getEyeDetector() {
	return this->eyesDetector;
}

int FaceDetector::detectFaces(const cv::_InputArray& image, std::vector<cv::Rect> & faces
		, cv::Mat* processedImage) const
{
	assert(this->isReady());
	return faceDetector.detectObjects(image, faces, processedImage);
}
int FaceDetector::detectFaces(const cv::_InputArray& image, std::vector<cv::Mat>& facesImg,
		bool grayscaleFaces, bool copyImages) const
{
	assert(this->isReady());
	return faceDetector.detectObjects(image, facesImg, grayscaleFaces, copyImages);
}

int FaceDetector::detectFaces(const cv::_InputArray& image, std::vector<Face>& faces) const {
	vector<Rect> facesRegion;
	this->detectFaces(image, facesRegion);
	for(size_t i=0; i < facesRegion.size(); ++i){
		Face face(facesRegion[i]);

		if(this->eyesDetector.isReady()){
			this->eyesDetector.detectEyes(image.getMat(), face);
		}

		faces.push_back(face);
	}

	return facesRegion.size();
}
