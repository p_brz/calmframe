/*
 * FaceAligner.cpp
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#include "FacePreprocessor.h"

#include <vector>
#include <assert.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
using namespace std;
using namespace cv;

#include "ImagePreprocessor.h"
#include "utils/GeomUtils.h"


const int FacePreprocessor::DEFAULT_FACE_WIDTH=70;
const int FacePreprocessor::DEFAULT_FACE_HEIGHT=70;
const double FacePreprocessor::DEFAULT_RELATIVE_EYES_HEIGHT=0.14;
const double FacePreprocessor::DEFAULT_RELATIVE_EYES_WIDTH=1.0 - (2*0.16);


FacePreprocessor::FacePreprocessor(FaceDetector* detector, cv::Size sizeFace,
		double relEyesHeight, double relEyesWidth)
: sizeFace(sizeFace)
, relativeEyesHeight(relEyesHeight)
, relativeEyesWidth(relEyesWidth)
, faceDetector(detector)
{
}

#include <iostream>

//@deprecated
void FacePreprocessor::prepareFaces(const cv::Mat& srcImage,
		std::vector<cv::Mat>& preparedFaces, bool grayscaleFaces, bool tryRotateImages)
{
	this->faceDetector->detectFaces(srcImage, preparedFaces,true, false);
	for(unsigned int i=0; i < preparedFaces.size(); ++i){
		Mat imgToPrepare = preparedFaces[i];
		Rect cropArea = Rect(0,0,imgToPrepare.cols-1,imgToPrepare.rows-1);
//		prepareFace(imgToPrepare, imgToPrepare, cropArea, true, tryRotateImages);
		prepareFace(imgToPrepare, imgToPrepare, cropArea, grayscaleFaces, tryRotateImages);
		preparedFaces[i] = imgToPrepare;
	}
}


//@deprecated
void FacePreprocessor::prepareFace(const cv::Mat& srcImage, cv::Mat & dstImage
		, const cv::Rect & facePoints, bool copyImage, bool tryRotateImages)
{
	cropFace(srcImage, dstImage, facePoints, copyImage);

	ImagePreprocessor::instance()->convertToGray(dstImage,dstImage);

//	if(tryRotateImages){
//		rotateImage(dstImage, dstImage, facePoints);
//	}
	if(this->sizeFace.width > 0 && this->sizeFace.height > 0){
		scaleImage(dstImage, this->sizeFace);
	}
}


cv::Mat FacePreprocessor::prepareFace(const cv::Mat& srcImage,
		const cv::Rect& facePoints, const cv::Point& leftEye,
		const cv::Point& rightEye)
{
	const ImagePreprocessor * imgPreprocessor = ImagePreprocessor::instance();
	Mat faceImage;
	cropFace(srcImage, faceImage, facePoints);
	imgPreprocessor->convertToGray(faceImage,faceImage);

	if(facePoints.contains(leftEye) && facePoints.contains(rightEye)){
		faceImage = alignFace(faceImage
				, leftEye - facePoints.tl(),rightEye - facePoints.tl(),this->sizeFace);
	}
	else{
		faceImage = imgPreprocessor->scaleImage(faceImage,this->sizeFace);
	}
	faceImage = this->equalizeFace(faceImage);
	faceImage = this->smoothFace(faceImage);
	this->applyMaskOn(faceImage);

	return faceImage;
}

cv::Mat FacePreprocessor::prepareFace(const cv::Mat& srcImage,
		const Face& face)
{
	GeomUtils * geom = GeomUtils::instance();
	Point2f lCenter, rCenter;

	if(face.hasLeftEye() && face.hasRightEye()){
		lCenter = geom->center(face.getLeftEye());
		rCenter = geom->center(face.getRightEye());
	}
	else{//Se não houver os dois olhos, define posições inválidas para os olhos. Desta forma, face não será rotacionada
		lCenter = Point(-1,-1);
		rCenter = Point(-1,-1);
	}

	return prepareFace(srcImage,face.getRegion(), lCenter,rCenter);
}
cv::Mat FacePreprocessor::alignFace(const cv::Mat& srcImage,
	const cv::Rect& facePoints, const cv::Point& leftEye,
	const cv::Point& rightEye, cv::Size size)
{
		return alignFace(srcImage(facePoints)
				, leftEye - facePoints.tl(), rightEye - facePoints.tl(), size);
}

cv::Mat FacePreprocessor::alignFace(const cv::Mat& faceImage,
	const cv::Point& lEyeCenter, const cv::Point& rEyeCenter, cv::Size size)
{
	if(size.height == 0 && size.width == 0){
		size = Size(faceImage.cols, faceImage.rows);
	}

	assert(size.width > 0);
	assert(size.height > 0);

	GeomUtils * geom = GeomUtils::instance();
	Point2f eyeCenter  = geom->center(lEyeCenter,rEyeCenter);
	double angle = geom->degreesBetween(lEyeCenter, rEyeCenter);
	double scale = getFaceScale(lEyeCenter, rEyeCenter);
	//Translação necessária para mover o ponto entre os olhos atual p/ o desejado
	Point2f translate  = getDesiredEyesCenter(size) - eyeCenter;

	return ImagePreprocessor::instance()->
			transformImage(faceImage,angle,scale,translate,size,eyeCenter);
}

template<typename T>
void FacePreprocessor::getDesiredEyesPosition(cv::Point_<T>& desiredLEye,
		cv::Point_<T>& desiredREye)
{
	//Espaço entre o olho e a borda da imagem
	const double EYES_OFFSET = (1.0 - this->relativeEyesWidth)*0.5;

	desiredLEye.x = (T)this->sizeFace.width * EYES_OFFSET;
	desiredREye.x = (T)this->sizeFace.width * (1.0 - EYES_OFFSET);

	desiredREye.y = desiredLEye.y = (T)this->sizeFace.height * this->relativeEyesHeight;
}

cv::Point2f FacePreprocessor::getDesiredEyesCenter(const cv::Size& sizeFace) {
	return Point2f(sizeFace.width * 0.5, sizeFace.height * this->relativeEyesHeight);
}

cv::Mat FacePreprocessor::smoothFace(cv::InputArray faceImg) {
	if(!faceImg.empty()){
		Mat smoothFace = Mat(faceImg.size(), faceImg.type());
		assert(faceImg.type() == CV_8UC1 || faceImg.type() == CV_8UC3);
		assert(faceImg.getMat().data != smoothFace.data);
		bilateralFilter(faceImg, smoothFace, 0 , 20, 20);
		return smoothFace;
	}
	return faceImg.getMat();
}

void FacePreprocessor::applyMaskOn(cv::InputOutputArray img,
		const cv::Scalar& color)
{
	// Draw a black-filled ellipse in the middle of the image.
	// First we initialize the mask image to white (255).
	Mat mask = Mat(img.size(), CV_8UC1, Scalar(255));
	double dw = img.cols();
	double dh = img.rows();
	Point faceCenter = Point( cvRound(dw * 0.5), cvRound(dh * 0.4) );
	Size size = Size( cvRound(dw * 0.5), cvRound(dh * 0.8) );
	ellipse(mask, faceCenter, size, 0, 0, 360, Scalar(0),FILLED);
	// Apply the elliptical mask on the face, to remove corners.
	// Sets corners to gray, without touching the inner face.
	img.setTo(color, mask);
}


double FacePreprocessor::getFaceScale(const cv::Point& lEyeCenter,
		const cv::Point& rEyeCenter)
{
	const double DESIRED_LEFT_EYE_X = 0.16;
//	const double DESIRED_RIGHT_EYE_X = (1.0 - DESIRED_LEFT_EYE_X);
	const double DESIRED_EYES_DIST = (1.0 - 2*DESIRED_LEFT_EYE_X);

	double desiredDistance = DESIRED_EYES_DIST * this->sizeFace.width;
	double distance = GeomUtils::instance()->distance(lEyeCenter, rEyeCenter);
	double scale = desiredDistance  / distance;

	assert(scale > 0);

	return scale;
}

uchar blend(uchar leftPixel, uchar rightPixel, float percent){
	return (uchar)cvRound((1.0f - percent) * leftPixel + percent * rightPixel);
}
Mat blendImages(const Mat & wholeFace, const Mat & leftSide, const Mat & rightSide){
	int w = wholeFace.cols;
	Mat faceImg = Mat(Size(wholeFace.cols, wholeFace.rows),wholeFace.type());

	int oneQuarter = w/4, secondQuarter = (w*2)/4, thirdQuarter = (w*3)/4;

	for(int i = 0; i < faceImg.rows; i++)
	{
		const uchar* leftIndex = leftSide.ptr<uchar>(i);
		const uchar* rightIndex = rightSide.ptr<uchar>(i);
		const uchar* wholeIndex = wholeFace.ptr<uchar>(i);

		for(int colIdx = 0; colIdx < faceImg.cols; colIdx++){
			uchar pixel;
			if(colIdx < oneQuarter){
				pixel = leftIndex[colIdx];
			}
			else if(colIdx < secondQuarter){
				float percent = (colIdx-oneQuarter)/(float)oneQuarter;
				pixel = blend(leftIndex[colIdx], wholeIndex[colIdx], percent);
			}
			else if(colIdx < thirdQuarter){
				float percent = (colIdx-secondQuarter)/(float)oneQuarter;
				pixel = blend(wholeIndex[colIdx], rightIndex[colIdx-secondQuarter], percent);
			}
			else{
				pixel = rightIndex[colIdx-secondQuarter];
			}
			faceImg.at<uchar>(i, colIdx) = pixel;
		}
	}

	return faceImg;
}

cv::Mat FacePreprocessor::equalizeFace(const cv::Mat&  srcImg,
		const cv::Rect& faceRegion)
{
	return this->equalizeFace(Mat(srcImg,faceRegion));
}

cv::Mat FacePreprocessor::equalizeFace(const cv::Mat&  img)
{
	Mat faceImg= img.clone();
	if(img.channels() > 1 ){
		ImagePreprocessor::instance()->convertToGray(faceImg, faceImg);
	}

	Mat equalizedFace;
	equalizeHist(faceImg, equalizedFace);

	int w = faceImg.cols, h = faceImg.rows;
	int midX = w/2;

	Mat leftSide;
	Mat rightSide;
	equalizeHist(faceImg(Rect(0,0, midX,h)), leftSide);
	equalizeHist(faceImg(Rect(midX,0, w-midX,h)), rightSide);

	return blendImages(equalizedFace,leftSide, rightSide);
//	return equalizedFace;
}

void FacePreprocessor::cropFace(const cv::Mat& srcImage, cv::Mat & dstImage
		, const cv::Rect & facePoints, bool copyImage) {
	dstImage = srcImage(facePoints);

	if(copyImage){
		dstImage = dstImage.clone();
	}

	assert(dstImage.cols == facePoints.width && dstImage.rows == facePoints.height);
}

void FacePreprocessor::scaleImage(cv::Mat& image, const cv::Size& sizeImg) {
	resize(image, image, sizeImg);
}

//void FacePreprocessor::convertToGray(const cv::_InputArray& srcImage, cv::Mat& dstImage) {
//	//FIXME: don't repeat yourself: retirado de FaceDetector
//	//FIXME: não necessariamente a imagem de origem será BGR!
//	cvtColor( srcImage, dstImage, COLOR_BGR2GRAY );
//	equalizeHist( dstImage, dstImage);
//}
