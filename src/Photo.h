/*
 * Photo.h
 *
 *  Created on: 15/05/2014
 *      Author: leobrizolara
 */

#ifndef PHOTO_H_
#define PHOTO_H_

#include <vector>
#include <string>
#include "Face.h"
#include <set>
class Photo {
private:
	int id;
	std::string imagePath;
	std::vector<Face> photoFaces;
	int facesCount;
public:
	static const int invalidPhotoId;
	static const int unknownFaceNumber;
public:
	Photo(const std::string & imgPath="");

	std::set<int> getPersonsId() const;

	void setImagePath(const std::string & imgPath);
	const std::string & getImagePath() const;
	void setFaces(const std::vector<Face> & faces);
	void setFacesToZero();
	void setFacesToUnknown();
	const std::vector<Face> & getFaces() const;
	int getFacesCount() const;
	const Face & getFace(unsigned int index) const;
	int getId() const;
	void setId(int id);
};

#endif /* PHOTO_H_ */
