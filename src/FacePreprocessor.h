/*
 * FaceAligner.h
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#ifndef FACEPREPROCESSOR_H_
#define FACEPREPROCESSOR_H_

#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include "FaceDetector.h"

class FacePreprocessor {
public:
	static const int DEFAULT_FACE_WIDTH;
	static const int DEFAULT_FACE_HEIGHT;
	static const double DEFAULT_RELATIVE_EYES_HEIGHT;
	static const double DEFAULT_RELATIVE_EYES_WIDTH;
private:
	/***/
	cv::Size sizeFace;

	/** Altura relativa da face em que os olhos serão mapeados*/
	double relativeEyesHeight;
	/** Largura relativa da face que será ocupada pelos olhos */
	double relativeEyesWidth;

	//@deprecated
	FaceDetector * faceDetector;

public:
	double getRelativeEyesHeight() const { return relativeEyesHeight;}
	double getRelativeEyesWidth () const { return relativeEyesWidth;}
	const cv::Size& getSizeFace () const { return sizeFace;}

	void setRelativeEyesHeight(double relativeEyesHeight) {this->relativeEyesHeight = relativeEyesHeight;}
	void setRelativeEyesWidth(double relativeEyesWidth)   {this->relativeEyesWidth = relativeEyesWidth;}
	void setSizeFace(const cv::Size& sizeFace) 			  {this->sizeFace = sizeFace;}
public:
	FacePreprocessor(FaceDetector * detector = NULL
			, cv::Size sizeFace = cv::Size(DEFAULT_FACE_WIDTH,DEFAULT_FACE_HEIGHT)
			, double relEyesHeight = DEFAULT_RELATIVE_EYES_HEIGHT
			, double relEyesWidth = DEFAULT_RELATIVE_EYES_WIDTH);
	/** @deprecated*/
	void prepareFaces(const cv::Mat& srcImage,std::vector<cv::Mat> & preparedFaces
			, bool grayscaleFaces=false, bool tryRotateImages = false);
	/** @deprecated*/
	void prepareFace(const cv::Mat& srcImage, cv::Mat & dstImage
				, const cv::Rect & facePoints, bool copyImage=false, bool tryRotateImages = false);

//	/** Extrai de srcImage a face definida por facePoints e preprocessa ela para facilitar o reconhecimento de faces.
//	 * */
	cv::Mat prepareFace(const cv::Mat& srcImage, const cv::Rect & facePoints
			, const cv::Point & leftEye,const cv::Point & rightEye);
	/** Extrai de srcImage a face definida por facePoints e preprocessa ela para facilitar o reconhecimento de faces.
	 * */
	cv::Mat prepareFace(const cv::Mat& srcImage, const Face & face);

	void cropFace(const cv::Mat& srcImage, cv::Mat & dstImage
			, const cv::Rect & facePoints, bool copyImage=false);
	void scaleImage(cv::Mat & image, const cv::Size & sizeImg);

	/** Alinha a imagem da face com base na posição dos olhos.
	 * 		Opcionalmente, pode escalar a imagem para tamanho 'size'.
		 * */
	cv::Mat alignFace(const cv::Mat& srcImage, const cv::Rect & facePoints
			, const cv::Point & leftEye,const cv::Point & rightEye, cv::Size size=cv::Size(0,0));

	/** Alinha a imagem da face com base na posição dos olhos.
	 * 		Opcionalmente, pode escalar a imagem para tamanho 'size'.
		 * */
	cv::Mat alignFace(const cv::Mat& faceImage
			, const cv::Point & leftEye,const cv::Point & rightEye, cv::Size size=cv::Size(0,0));


	cv::Mat equalizeFace(const cv::Mat&  srcImg);
	cv::Mat equalizeFace(const cv::Mat&  srcImg, const cv::Rect & faceRegion );

	cv::Mat smoothFace(cv::InputArray faceImg);

	void applyMaskOn(cv::InputOutputArray src, const cv::Scalar & color=cv::Scalar(128));
protected:
	cv::Point2f getDesiredEyesCenter(const cv::Size & sizeFace);
	template<typename T>
	void getDesiredEyesPosition(cv::Point_<T> & desiredLEye, cv::Point_<T> & desiredREye);
	double getFaceScale(const cv::Point & lEyeCenter, const cv::Point & rEyeCenter);
};

#endif /* FACEALIGNER_H_ */
