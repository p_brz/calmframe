/*
 * FaceIdentifier.h
 *
 *  Created on: 20/04/2014
 *      Author: leobrizolara
 */

#ifndef FACEIDENTIFIER_H_
#define FACEIDENTIFIER_H_

#include <opencv2/core.hpp>
#include <opencv2/contrib.hpp>

#include "Face.h"

class FaceIdentifier {
private:
	bool ready;
	bool useFaceValidation;
	double faceValidationThreshold;
	cv::Ptr<cv::FaceRecognizer> recognizer;
public:
	FaceIdentifier();

	void train(cv::InputArrayOfArrays src, cv::InputArray labels);
	int identify(const cv::_InputArray & faceImg, double * confidence = NULL) const;
public:
	bool isReady()const{
		return ready;
	}

	const cv::Ptr<cv::FaceRecognizer>& getRecognizer() const {
		return recognizer;
	}

	void setRecognizer(const cv::Ptr<cv::FaceRecognizer>& recognizer) {
		this->recognizer = recognizer;
	}

	//Face validation
	bool isAKnownFace(const cv::Mat & srcImg, const Face & face) const;
	bool isAKnownFace(const cv::Mat & faceImg) const;

	cv::Mat reconstructFace(const cv::Mat& faceImg) const;

	bool isUsingFaceValidation() const {
		return useFaceValidation;
	}
	void setFaceValidationUse(bool useFaceReconstruction) {
		this->useFaceValidation = useFaceReconstruction;
	}
	double getValidationThreshold() const {
		return useFaceValidation;
	}
	void setValidationThreshold(double validationThreshold) {
		this->faceValidationThreshold = validationThreshold;
	}

};

#endif /* FACEIDENTIFIER_H_ */
