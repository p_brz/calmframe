/*
 * VideoPlayer.h
 *
 *  Created on: 17/04/2014
 *      Author: leobrizolara
 */

#ifndef VIDEOPLAYER_H_
#define VIDEOPLAYER_H_

#include <string>
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O
#include "VideoSource.hpp"

class VideoPlayer : public VideoSource{
private:
	std::string videoWindowName;
	bool useTrackbar;
	std::string trackbarName;
	int trackbarPosition;
	bool playing;
	unsigned int framePosition;
	unsigned int frameCount;
	unsigned int videoFPS;
	cv::VideoCapture video;
	cv::Mat currentFrame;
	std::string videoFilename;
public:
	static void staticTrackbarChange(int trackPosition, void * videoPlayer);
//	static const char * defaultWindowName;
//	static const char * defaultTrackbarName;
protected:
	void trackbarChange(int trackPosition);
public:
//	VideoPlayer(const char * videoFilename = NULL,
//			const char * windowName = defaultWindowName, bool useTrackbar=true);
	VideoPlayer(const std::string & videoFilename = "");

	bool start();
	bool start(const std::string & filename);
	void pause();
	void resume();
	void stop();
	bool gotoFrame(unsigned int framePosition);
	bool update();

	bool isPlaying() const;
	cv::VideoCapture & getVideo();

//	void enableTrackbar(const std::string & windowName, const std::string & trackbarName = "trackbar");
	bool enableTrackbar(const std::string & windowName, const std::string & trackbarName = "trackbar");
//	void hideTrackbar();
//	void showTrackbar();
//	bool trackbarIsVisible() const;
//	void setTrackbarVisibility(bool visibility);

//	const char * getVideoWindowName() const;
//	void setVideoWindowName(const char * windowName);

	cv::Mat & getCurrentFrame();
	int getFrameCount() const;
	int getVideoFPS() const;
};

#endif /* VIDEOPLAYER_H_ */
