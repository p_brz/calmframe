/*
 * VideoSourceCam.cpp
 *
 *  Created on: 30/05/2014
 *      Author: leobrizolara
 */

#include <VideoSourceCam.h>
#include <opencv2/core.hpp>

using namespace cv;
using namespace std;

VideoSourceCam::VideoSourceCam(int cam)
	: camNumber(cam)
	, frameCount(0)
	, playing(false)
	, currentFPS(0)
{
}

#include <iostream>

bool VideoSourceCam::start() {
	cout << "camNumber: "<<camNumber << endl;
	if(!this->video.isOpened() && camNumber >= 0){
		this->playing = this->video.open(camNumber);
		cout << "opened: " << this->playing << endl;
//		currentFPS = (unsigned int)this->video.get(CAP_PROP_FPS);
	}
	return this->video.isOpened();
}

void VideoSourceCam::pause() {
	this->playing = false;
}

void VideoSourceCam::resume() {
	this->playing = true;
}

void VideoSourceCam::stop() {
	this->pause();
	this->video.release();
	this->currentFrame = Mat();
	this->currentFPS = 0;
}

bool VideoSourceCam::update() {

	if(this->isPlaying()){
		if(video.read(this->currentFrame) && !currentFrame.empty()){
			++frameCount;
//			currentFPS = (unsigned int)this->video.get(CAP_PROP_FPS);
			return true;
		}
		else{
			currentFPS = 0;
		}
	}

	return false;
}

bool VideoSourceCam::isPlaying() const {
	return this->playing && this->video.isOpened();
}

int VideoSourceCam::getFrameCount() const {
	return frameCount;
}

int VideoSourceCam::getVideoFPS() const {
	return currentFPS;
}

cv::Mat& VideoSourceCam::getCurrentFrame() {
	return this->currentFrame;
}

//const cv::VideoCapture& VideoSourceCam::getVideo() const {
//	return this->video;
//}
cv::VideoCapture& VideoSourceCam::getVideo() {
	return this->video;
}
