/*
 * VideoPlayer.cpp
 *
 *  Created on: 17/04/2014
 *      Author: leobrizolara
 */

#include "VideoPlayer.h"
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O

using namespace std;
using namespace cv;

//const char * VideoPlayer::defaultWindowName = "Video";
//const char * VideoPlayer::defaultTrackbarName = "trackbar";

void VideoPlayer::staticTrackbarChange(int trackPosition, void* videoPlayer) {
	VideoPlayer * somePlayer = (VideoPlayer*)videoPlayer;
	somePlayer->trackbarChange(trackPosition);
}

void VideoPlayer::trackbarChange(int trackPosition) {
	this->gotoFrame(trackPosition);
}


std::string videoWindowName;
bool useTrackbar;
std::string trackbarName;
int trackbarPosition;
bool playing;
unsigned int framePosition;
unsigned int frameCount;
unsigned int videoFPS;

VideoPlayer::VideoPlayer(const std::string & filename)
	: useTrackbar(false)
	, trackbarPosition(0)
	, playing(false)
	, framePosition(0)
	, frameCount(0)
	, videoFPS(0)
	, videoFilename(filename)
{
//	if(videoFilename.length() > 0){
//		this->start(videoFilename);
//	}
}



bool VideoPlayer::start() {
	if(!this->video.isOpened()){
		if(videoFilename.length() >0 && this->video.open(this->videoFilename)){
			playing = true;
			framePosition = 0;
			trackbarPosition = 0;
			frameCount = (unsigned int)this->video.get(CAP_PROP_FRAME_COUNT);
			videoFPS = (unsigned int)this->video.get(CAP_PROP_FPS);
		}
	}
	return this->video.isOpened();
}
bool VideoPlayer::start(const std::string & filename) {
	this->stop();
	this->videoFilename = filename;
	return start();
}


void VideoPlayer::stop() {
	this->video.release();
	this->playing = false;
	this->framePosition = 0;
	this->trackbarPosition = 0;
	this->videoFPS=0;
	this->frameCount=0;
	useTrackbar = false;
	if(this->currentFrame.cols > 0 || this->currentFrame.rows > 0){
//			Define todos os pontos com preto
		this->currentFrame = Scalar(0,0,0);
	}
}

bool VideoPlayer::enableTrackbar(const std::string & windowName, const std::string & trackbarName){
	this->videoWindowName = windowName;
	this->trackbarName = trackbarName;
	useTrackbar = true;
	createTrackbar(this->trackbarName, this->videoWindowName, &trackbarPosition
			, this->frameCount,VideoPlayer::staticTrackbarChange,this);

	return true;
}

void VideoPlayer::pause() {
	this->playing = false;
}

void VideoPlayer::resume() {
	this->playing = true;
}

bool VideoPlayer::gotoFrame(unsigned int newPosition) {
	if(newPosition != this->framePosition && this->frameCount > newPosition && this->video.isOpened()){
		this->video.set(CAP_PROP_POS_FRAMES, newPosition);
		this->framePosition = newPosition;
		return true;
	}
	return false;
}

bool VideoPlayer::update() {
	if(this->isPlaying()){
		if(video.read(this->currentFrame) && !currentFrame.empty()){
			++framePosition;
			if(useTrackbar){
				setTrackbarPos(this->trackbarName, this->videoWindowName, framePosition);
			}
			return true;
		}
	}

	return false;
}

cv::Mat& VideoPlayer::getCurrentFrame(){
	return this->currentFrame;
}
int VideoPlayer::getFrameCount() const {
	return this->framePosition;
}

bool VideoPlayer::isPlaying() const {
	return this->video.isOpened() && this->playing;
}

cv::VideoCapture& VideoPlayer::getVideo() {
	return this->video;
}

int VideoPlayer::getVideoFPS() const {
	return this->videoFPS;
}
