/*
 * VideoSource.h
 *
 *  Created on: 30/05/2014
 *      Author: leobrizolara
 */

#ifndef VIDEOSOURCE_H_
#define VIDEOSOURCE_H_

//TODO: desacoplar de opencv
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

class VideoSource {
public:
	virtual ~VideoSource(){}

	virtual bool start()=0;
	virtual void pause()=0;
	virtual void resume()=0;
	virtual void stop()=0;
	virtual bool update() = 0;

	virtual bool isPlaying() const = 0;

	virtual int getFrameCount() const = 0;
	virtual int getVideoFPS() const = 0;
	virtual cv::Mat & getCurrentFrame()=0;
//	virtual cv::VideoCapture & getVideo()=0;
	//FIXME: desta forma o video pode ser alterado e ficar com estado inconsistente com a classe
	virtual cv::VideoCapture & getVideo() =0;
	/** Cria uma trackbar se a fonte de video suportar isto*/
	virtual bool enableTrackbar(const std::string & windowName, const std::string & trackbarName = "trackbar")
	{
		return false;
	}
};

#endif /* VIDEOSOURCE_H_ */
