/*
 * EyeDetector.h
 *
 *  Created on: 23/04/2014
 *      Author: leobrizolara
 */

#ifndef EYEDETECTOR_H_
#define EYEDETECTOR_H_

#include <vector>
#include <string>
#include <opencv2/core.hpp>
#include "ObjectDetector.h"
#include "Face.h"

class EyeDetector {
private:
	double searchOffsetX;
	double searchOffsetY;
	double searchWidth;
	double searchHeight;

	ObjectDetector eyeDetector;
public:
	double getSearchHeight () const;
	double getSearchOffsetX() const;
	double getSearchOffsetY() const;
	double getSearchWidth  () const;
	void   setSearchHeight (double searchHeight);
	void   setSearchOffsetX(double searchOffsetX);
	void   setSearchOffsetY(double searchOffsetY);
	void   setSearchWidth  (double searchWidth);
public:
	EyeDetector(const std::string & eyesCascadeFilename = "");
	bool setup(const std::string & eyesCascadeFilename);
//	bool setup(const std::string & leftCascadeFilename, const std::string & rightCascadeFilename);

	bool isReady() const;

	int detectEyesCandidates(const cv::Mat & srcImage, const cv::Rect & facePoints
			, std::vector<cv::Rect> & eyes) const;
	int detectEyes(const cv::Mat & srcImage, const cv::Rect & facePoints
			, cv::Rect & leftEye, cv::Rect & rightEye) const;
	int detectEyes(const cv::Mat & srcImage, Face & face) const;

	void extractEyesRegion(const cv::Rect & facePoints
			, cv::Rect & leftEyeRegion, cv::Rect & rightEyeRegion) const;
protected:
	bool detectEye(cv::Rect & eye, const cv::Mat& srcImage,
			const cv::Rect& eyeSchReg, const cv::Size& schRegSize) const;
};

#endif /* EYEDETECTOR_H_ */
