/*
 * PersonMonitorObserver.h
 *
 *  Created on: 20/05/2014
 *      Author: leobrizolara
 */

#ifndef PERSONMONITOROBSERVER_H_
#define PERSONMONITOROBSERVER_H_

#include <vector>
#include "Person.h"

class PersonMonitorObserver {
public:
	virtual ~PersonMonitorObserver(){}
	virtual void onPersonsDetected(const std::vector<Person> & persons)=0;
};

#endif /* PERSONMONITOROBSERVER_H_ */
