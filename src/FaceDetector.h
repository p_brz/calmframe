/*
 * FaceDetector.h
 *
 *  Created on: 15/04/2014
 *      Author: leobrizolara
 */

#ifndef FACEDETECTOR_H_
#define FACEDETECTOR_H_

#include <vector>
#include <string>
#include <opencv2/core.hpp>
#include "ObjectDetector.h"
#include "EyeDetector.h"
#include "Face.h"

class FaceDetector {
private:
	ObjectDetector faceDetector;
	EyeDetector eyesDetector;
public:
	FaceDetector(const std::string & faceCascadeFilename = "", const std::string & eyesCascadeFilename = "");
	bool setup(const std::string & faceCascadeFilename);
	bool setupEyesDetector(const std::string & eyesCascadeFilename);
	bool isReady() const;
	bool eyesDetectorIsReady() const;
	EyeDetector & getEyeDetector();

	/*TODO: remover funcionalidades p/ retornar imagem "pré-processada",
	 * mover responsabilidade para clientes/outra classe	*/
	int detectFaces(const cv::_InputArray& image, std::vector<cv::Rect> & faces
			, cv::Mat* processedImage = NULL) const;

	/** @deprecated
	 * FIXME: remover método "detectFaces(const cv::_InputArray& image, std::vector<cv::Mat> & facesImg ..."*/
	int detectFaces(const cv::_InputArray& image, std::vector<cv::Mat> & facesImg
			, bool grayscaleFaces = false, bool copyImages = false) const;
	int detectFaces(const cv::_InputArray& image, std::vector<Face> & faces) const;
};

#endif /* FACEDETECTOR_H_ */
