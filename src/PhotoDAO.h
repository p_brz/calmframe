/*
 * PhotosDAO.h
 *
 *  Created on: 20/05/2014
 *      Author: leobrizolara
 */

#ifndef PHOTO_DAO_H_
#define PHOTO_DAO_H_

#include <vector>

#include "Photo.h"
#include "Person.h"

class PhotoDAO {
public:
	virtual ~PhotoDAO(){};
	virtual std::vector<Photo> listAll() const =0;
	virtual std::vector<Photo> findPhotosByPerson(const Person & person) const =0;
	virtual void create(const Photo & photo) =0;
	virtual void create(const std::vector<Photo> & photo) =0;
	virtual void update(const Photo & photo) =0;
	virtual void update(const std::vector<Photo> & photo) =0;
	virtual void removeAll() =0;
};

#endif /* PHOTOSDAO_H_ */
