/*
 * App.h
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#ifndef APP_H_
#define APP_H_


class App {
public:
    static const char * windowName;

	int delay;
protected:
	static const int KEY_ESC;
public:
	App();
	virtual ~App(){};
	int run(int argc, char * argv[]);
	virtual bool setup(int argc, char * argv[]){ return true;};
	virtual bool update(){ return false;};
	virtual void keyPressed(int key){};
};

#endif /* APP_H_ */
