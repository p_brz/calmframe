/*
 * ImageComparator.cpp
 *
 *  Created on: 23/05/2014
 *      Author: leobrizolara
 */

#include <assert.h>
#include <iostream>
#include <cmath>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;
using namespace std;

#include <ImageComparator.h>
#include <cfloat>
//#include <cstdint>

double ImageComparator::getMaxPixelValue(const cv::Mat& img) const{
	double maxPixelValue;
	switch(img.depth()){
	case CV_8U: maxPixelValue = 255;
		break;
	case CV_8S: maxPixelValue = 127;
		break;
	case CV_16U: maxPixelValue =  65535;
		break;
	case CV_16S: maxPixelValue = 32767;
		break;
	case CV_32S: maxPixelValue = 2147483647;
		break;
	case CV_32F: maxPixelValue =  FLT_MAX;
		break;
	case CV_64F: maxPixelValue =  DBL_MAX;
		break;
	}


	return maxPixelValue;
}

//double ImageComparator::getSimilarity(const cv::Mat& img1, const cv::Mat& img2) const
//{
//	if(img1.empty() && img2.empty()){
//		return 1.0;
//	}
//
//	double similarity = -1;
//	if(img1.rows == img2.rows && img1.cols == img2.cols){
//		//diferença máxima entre dois pixels
//		double maxDif = this->getMaxPixelValue(img1);
//
//		// Calculate the L2 relative error between the 2 images.
//		double errorL2 = norm(img1, img2, NORM_L2);
//		// Scale the value since L2 is summed across all pixels.
////		similarity = errorL2 / (double)(img1.rows * img2.cols);
//		similarity = errorL2 / (sqrt(img1.rows * img2.cols) * maxDif);
//		//"Inverte" escala de similarity para que imagens mais semelhantes possuam valores próximos de 1
//		similarity = 1.0- similarity;
//
//		//Nunca deveria retornar um valor negativo ou maior que 1.0
//		if(similarity < 0.0 || similarity > 1.0){
//			cerr << "Found invalid similarity value: "<<similarity<<endl;
//			cerr << "A validy similarity should not be negative or greater than 1"<<endl;
//			assert(similarity >= 0.0);
//			assert(similarity <= 1.0);
//		}
//	}
//	return similarity;
//}


/* código retirado de : http://docs.opencv.org/doc/tutorials/highgui/video-input-psnr-ssim/video-input-psnr-ssim.html*/
static Scalar getMSSIM(const Mat& i1, const Mat& i2) {
	const double C1 = 6.5025, C2 = 58.5225;
	/***************************** INITS **********************************/
	int d = CV_32F;

	Mat I1, I2;
	i1.convertTo(I1, d);           // cannot calculate on one byte large values
	i2.convertTo(I2, d);

	Mat I2_2 = I2.mul(I2);        // I2^2
	Mat I1_2 = I1.mul(I1);        // I1^2
	Mat I1_I2 = I1.mul(I2);        // I1 * I2

	/***********************PRELIMINARY COMPUTING ******************************/

	Mat mu1, mu2;   //
	GaussianBlur(I1, mu1, Size(11, 11), 1.5);
	GaussianBlur(I2, mu2, Size(11, 11), 1.5);

	Mat mu1_2 = mu1.mul(mu1);
	Mat mu2_2 = mu2.mul(mu2);
	Mat mu1_mu2 = mu1.mul(mu2);

	Mat sigma1_2, sigma2_2, sigma12;

	GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;

	GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;

	GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;

	///////////////////////////////// FORMULA ////////////////////////////////
	Mat t1, t2, t3;

	t1 = 2 * mu1_mu2 + C1;
	t2 = 2 * sigma12 + C2;
	t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

	t1 = mu1_2 + mu2_2 + C1;
	t2 = sigma1_2 + sigma2_2 + C2;
	t1 = t1.mul(t2);   // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

	Mat ssim_map;
	divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

	Scalar mssim = mean(ssim_map); // mssim = average of ssim map
	return mssim;
}
double ImageComparator::getSimilarity(const cv::Mat& img1, const cv::Mat& img2) const
{
	if(img1.empty() && img2.empty()){
		return 1.0;
	}

	double similarity = -1;
	if(img1.rows == img2.rows && img1.cols == img2.cols && img1.channels() == img2.channels()){
		Scalar mssim = getMSSIM(img1,img2);
		//Fazendo media do mssim p/ cada canal da imagem
		similarity =0;
		unsigned int channelsCount = img1.channels();
		for(size_t i=0; i < channelsCount; ++i){
			similarity += mssim[i];
		}
		similarity = similarity/channelsCount;
	}
	return similarity;
}

