/*
 * ImagePreprocessor.cpp
 *
 *  Created on: 02/05/2014
 *      Author: leobrizolara
 */

#include <ImagePreprocessor.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;


ImagePreprocessor* ImagePreprocessor::instance() {
	static ImagePreprocessor singleton;
	return &singleton;
}

void ImagePreprocessor::preprocessImage(
		const cv::_InputArray& src, cv::OutputArray dst)
{
	convertToGray(src,dst);
	equalize(dst,dst);
}

void ImagePreprocessor::convertToGray(const cv::_InputArray& src,
		cv::OutputArray dst) {
	//FIXME: considerar diferentes formatos de entrada (que não BGR)
	if(src.channels() > 1){
	  //nada garante que image esteja no formato BGR (apesar deste ser o padrão)
	  cv::cvtColor(src, dst, COLOR_BGR2GRAY);
	}
	else{
		src.copyTo(dst);
	}
}

void ImagePreprocessor::equalize(const cv::_InputArray& src,
		cv::OutputArray dst)
{
	  equalizeHist( src, dst );
}

cv::Mat ImagePreprocessor::transformImage(cv::InputArray src, double rotation,
		double scale, const cv::Point2f &translate, const cv::Size& dstSize) const
{
	Size srcSize = src.size();
	Size finalSize = dstSize;
	if(dstSize.area() <= 0 ){
		finalSize = srcSize;
	}
	return transformImage(src,rotation,scale,translate, finalSize
			, Point2f(srcSize.width/2,srcSize.height/2));
}

cv::Mat ImagePreprocessor::transformImage(cv::InputArray src, double rotation,
		double scale, const cv::Point2f & translate, const cv::Size& dstSize,
		const cv::Point2f & rotCenter) const
{
	Mat rot_mat = getRotationMatrix2D(rotCenter,rotation,scale);

	//Aplicar translação
	rot_mat.at<double>(0, 2) += translate.x;
	rot_mat.at<double>(1, 2) += translate.y;
//		rot_mat.at<double>(0, 2) += desiredEyesCenter.x - rotCenter.x;
//		rot_mat.at<double>(1, 2) += desiredEyesCenter.y - rotCenter.y;
	Mat dstImg;
	warpAffine(src, dstImg, rot_mat, dstSize);

	return dstImg;
}

cv::Mat ImagePreprocessor::scaleImage(cv::InputArray src, double scale) const {
	return this->scaleImage(src, Size(src.cols() * scale, src.rows() * scale));
}

cv::Mat ImagePreprocessor::scaleImage(cv::InputArray src, const cv::Size& finalSize) const {
	Mat dst;
	resize(src,dst,finalSize);
	return dst;
}
