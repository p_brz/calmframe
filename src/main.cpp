/*
 * main.cpp
 *
 *  Created on: 07/04/2014
 *      Author: leobrizolara
 */

#include "playVideo.h"
#include "detectFaceOnVideo.h"
#include "videoPlayerMain.h"
#include "VideoFaceDetectorApp.h"
#include "FaceIdentifierApp.h"
#include "FacePreparerApp.h"
#include "eyeDetection.h"
#include "App.h"
#include "TestApp.h"
#include "PhotoFrameApp.h"
#include "FacePreprocessorApp.h"

int mainApp(App * app, int argc, char ** argv){
	return app->run(argc,argv);
}

int main(int argc, char * argv[]){
//	return playVideoMain(argc, argv);
//	return videoPlayerMain(argc, argv);
//	return detectFaceOnVideoMain(argc, argv);
//	return mainFacePreparer(argc, argv);
//	return mainFaceIdentifier(argc, argv);
//	return mainEyeDetection(argc,argv);
//	return mainVideoFaceDetectorApp(argc, argv);
//	return mainApp(new TestApp(),argc,argv);
	return mainApp(new PhotoFrameApp(),argc,argv);
//	return mainApp(new VideoFaceDetectorApp(), argc,argv);
//	return mainApp(new FacePreprocessorApp(), argc,argv);
}



