/*
 * Person.cpp
 *
 *  Created on: 27/04/2014
 *      Author: leobrizolara
 */

#include <Person.h>

Person::Person(int someId)
	: id(someId)
{
}

int Person::getId() const {
	return id;
}

const std::string& Person::getName() const {
	return name;
}

void Person::setName(const std::string& name) {
	this->name = name;
}

void Person::setId(int id) {
	this->id = id;
}
