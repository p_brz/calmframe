/*
 * ImageLoader.h
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#ifndef IMAGELOADER_H_
#define IMAGELOADER_H_

#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

class ImageLoader {
public:
	bool loadImages(const std::string & filename, std::vector<cv::Mat> & images,
			std::vector<int> & labels, int imreadFlags=cv::IMREAD_ANYCOLOR);
};

#endif /* IMAGELOADER_H_ */
