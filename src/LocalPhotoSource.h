/*
 * LocalPhotoSource.h
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#ifndef LOCALPHOTOSOURCE_H_
#define LOCALPHOTOSOURCE_H_

#include <string>
#include <vector>

class LocalPhotoSource {
private:
	unsigned int currentPhotoIndex;
	std::string rootPath;
	std::vector<std::string> photos;
public:
	LocalPhotoSource(const std::string & photoRootPath = "");
	//Getters and setters
	const std::string& getRootPath() const;
	void setRootPath(const std::string& rootPath);

	bool loadPhotos(bool recursive = false);
	unsigned int photoCount() const;
	unsigned int getCurrentPhotoNumber() const;
	void nextPhoto();
	void previousPhoto();
	void setCurrentPhoto(unsigned int photoNumber);
	std::string getCurrentPhotoPath() const;
	std::string getPhotoPath(unsigned int photoNumber) const;
	std::string getCurrentPhotoAbsolutePath(const std::string & basePath="") const;
protected:
//	void loadPhotosRecursive(std::vector<std::string> & files);
	bool loadPhotosRecursive(const std::string & rootFile, std::vector<std::string> & files);
	void filterDirectories(std::vector<std::string> & files, const std::string & basePath="") const;
	void filterPhotoFiles(std::vector<std::string> & files) const;
};

#endif /* LOCALPHOTOSOURCE_H_ */
