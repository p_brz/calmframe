/*
 * ImageLoader.cpp
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#include "ImageLoader.h"

#include <string>
#include <vector>
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "utils/CsvReader.h"

using namespace std;
using namespace cv;


//static bool read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
//    std::ifstream file(filename.c_str(), ifstream::in);
//    if (!file) {
//        return false;
//    }
//    string line, path, classlabel;
//    while (getline(file, line)) {
//        stringstream liness(line);
//        getline(liness, path, separator);
//        getline(liness, classlabel);
//        if(!path.empty() && !classlabel.empty()) {
//            images.push_back(imread(path, 0));
//            labels.push_back(atoi(classlabel.c_str()));
//        }
//    }
//	return true;
//}

//bool ImageLoader::loadImages(const std::string & filename, std::vector<cv::Mat> & images,
//		std::vector<int> & labels)
//{
//	if(!read_csv(filename, images, labels)){
//		return false;
//	}
//	return true;
//}

#define IMAGEPATH_COL 0
#define LABEL_COL 1
#define SEPARATOR ';'

bool ImageLoader::loadImages(const std::string & filename, std::vector<cv::Mat> & images,
		std::vector<int> & labels, int imreadFlags)
{
	CsvReader reader(';');
	reader.open(filename);
	if(reader.isOpen()){
		while(reader.nextRow()){
			images.push_back(imread(reader.getCurrentRow().get(IMAGEPATH_COL),imreadFlags));
			labels.push_back(atoi(reader.getCurrentRow().get(LABEL_COL).c_str()));
		}

		reader.close();

		return true;
	}
	return false;
}
