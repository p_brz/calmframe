/*
 * ImagePreprocessor.h
 *
 *  Created on: 02/05/2014
 *      Author: leobrizolara
 */

#ifndef IMAGEPREPROCESSOR_H_
#define IMAGEPREPROCESSOR_H_

#include <opencv2/core.hpp>

class ImagePreprocessor {
public:
	static ImagePreprocessor * instance();
	static void preprocessImage(const cv::_InputArray& src, cv::OutputArray dst);
	static void convertToGray(const cv::_InputArray& src, cv::OutputArray dst);
	static void equalize(const cv::_InputArray& src, cv::OutputArray dst);
	cv::Mat transformImage(
			cv::InputArray src,
			double rotation=0, double scale=1,const cv::Point2f &translate = cv::Point2f(0,0),
			const cv::Size & dstSize=cv::Size(0,0)) const;
	cv::Mat transformImage(
			cv::InputArray src,
			double rotation, double scale, const cv::Point2f & translate,
			const cv::Size & dstSize, const cv::Point2f &  rotCenter) const;
	cv::Mat scaleImage(cv::InputArray src, double scale) const;
	cv::Mat scaleImage(cv::InputArray src, const cv::Size & finalSize) const;
};

#endif /* IMAGEPREPROCESSOR_H_ */
