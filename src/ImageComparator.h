/*
 * ImageComparator.h
 *
 *  Created on: 23/05/2014
 *      Author: leobrizolara
 */

#ifndef IMAGECOMPARATOR_H_
#define IMAGECOMPARATOR_H_
#include <opencv2/core.hpp>

class ImageComparator {
public:
	double getSimilarity(const cv::Mat & img1, const cv::Mat & img2) const;
protected:
	double getMaxPixelValue(const cv::Mat& img) const;
};

#endif /* IMAGECOMPARATOR_H_ */
