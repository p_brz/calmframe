/*
 * LocalPhotoDAO.cpp
 *
 *  Created on: 20/05/2014
 *      Author: leobrizolara
 */

#include <LocalPhotoDAO.h>
#include <assert.h>

#include "Photo.h"

using namespace std;

const int LocalPhotoDAO::initialPhotoId = 0;

LocalPhotoDAO::LocalPhotoDAO(LocalPhotoSource & source)
	: lastPhotoId(initialPhotoId)
{
	this->copyFromPhotoSource(source);
}

LocalPhotoDAO::LocalPhotoDAO(const std::string& rootPath)
	:lastPhotoId(initialPhotoId)
{
	LocalPhotoSource localSource(rootPath);
	if(localSource.loadPhotos(true)){
		this->copyFromPhotoSource(localSource);
	}
}

std::vector<Photo> LocalPhotoDAO::listAll() const {
	return this->photosCache;
}

std::vector<Photo> LocalPhotoDAO::findPhotosByPerson(
		const Person& person) const {
	//FIXME: IMPLEMENTAR!!
	return this->photosCache;
}


void LocalPhotoDAO::create(const Photo& photo) {
	Photo toInsertPhoto = Photo(photo);
	toInsertPhoto.setId(++this->lastPhotoId);
	this->photosCache.push_back(toInsertPhoto);
}

void LocalPhotoDAO::create(const std::vector<Photo>& photos) {
	for(size_t i=0; i<photos.size(); ++i){
		this->create(photos[i]);
	}
}

void LocalPhotoDAO::update(const Photo& photo) {
	int photoIndex = findPhoto(photo.getId());
	if(photoIndex >= 0){
		this->photosCache[photoIndex] = photo;
	}
}

void LocalPhotoDAO::update(const std::vector<Photo>& photos) {
	for(size_t i=0; i<photos.size(); ++i){
		this->update(photos[i]);
	}
}


int LocalPhotoDAO::findPhoto(int photoId) {
	int index=-1;
	for(size_t i=0; i<this->photosCache.size(); ++i){
		if(this->photosCache[i].getId() == photoId){
			return i;
		}
	}
	return index;
}

void LocalPhotoDAO::removeAll() {
	this->photosCache.clear();
}

void LocalPhotoDAO::copyFromPhotoSource(LocalPhotoSource& photoSource) {
	unsigned int numPhotos = photoSource.photoCount();
	if(numPhotos > 0){
		vector<Photo> photos;

		unsigned int initialPhotoNumber = photoSource.getCurrentPhotoNumber();
		for(unsigned int i=0; i < numPhotos; ++i){
			photoSource.setCurrentPhoto(i);
			photos.push_back(Photo(photoSource.getCurrentPhotoPath()));
		}
		photoSource.setCurrentPhoto(initialPhotoNumber);

//		this->photosCache= photos;
		this->create(photos);
		assert(photosCache.size() == photoSource.photoCount());
	}
}
