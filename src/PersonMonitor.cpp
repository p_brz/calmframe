/*
 * PersonMonitor.cpp
 *
 *  Created on: 19/05/2014
 *      Author: leobrizolara
 */

#include <PersonMonitor.h>
#include <assert.h>
#include <opencv2/core.hpp>
#include "Face.h"
#include "ImagePreprocessor.h"

using namespace std;
using namespace cv;

PersonMonitor::PersonMonitor(cv::VideoCapture* capture,
		FaceIdentifier* identifier, FaceDetector * detector
		, FacePreprocessor * preprocessor)
	: personCapture(capture)
	, faceIdentifier(identifier)
	, faceDetector(detector)
	, facePreprocessor(preprocessor)
{
}

bool PersonMonitor::update() {

	if(this->isReady()){
		Mat frame;
		if(this->personCapture->retrieve(frame)){
			//FIXME: encapsular processo de detecção/reconhecimento de faces
			vector<Face> faces;
			vector<Person> persons;

			Mat preparedImage = prepareImage(frame);

			if(this->faceDetector->detectFaces(preparedImage, faces) > 0){
				for(size_t i=0; i < faces.size(); ++i){

					Mat preparedFace;
					if(this->facePreprocessor != NULL){
						preparedFace = this->facePreprocessor->prepareFace(preparedImage,faces[i]);
					}
					else{
						preparedFace = preparedImage(faces[i].getRegion());
					}

					int id = this->faceIdentifier->identify(preparedFace);
					if(id >=0){
						//TODO: person deveria ser obtida de banco de dados (ou similar)
						persons.push_back(Person(id));
						faces[i].setPersonId(id);
					}
				}
//				this->notify(persons);
			}

			if(faces.size() > 0 ){
				this->notify(faces);
			}

			if(persons.size() > 0){
				notify(persons);
			}

			return true;
		}
	}
	return false;
}


void PersonMonitor::notify(const std::vector<Person>& persons) const {
	for(ObserversIterator iterator=observers.begin(); iterator != observers.end(); ++iterator){
		(*iterator)->onPersonsDetected(persons);
	}
}

void PersonMonitor::registerObserver(PersonMonitorObserver* observer) {
	assert(observer != NULL);
	if(observer != NULL){
		this->observers.insert(observer);
	}
}

void PersonMonitor::unregisterObserver(PersonMonitorObserver* observer) {
	//TODO:
}

bool PersonMonitor::isRegistered(PersonMonitorObserver* observer) {
	return this->observers.find(observer) != this->observers.end();
}

void PersonMonitor::registerObserver(PersonFacesObserver* observer) {
	assert(observer != NULL);
	if(observer != NULL){
		this->facesObservers.insert(observer);
	}
}

void PersonMonitor::unregisterObserver(PersonFacesObserver* observer) {
}

bool PersonMonitor::isRegistered(PersonFacesObserver* observer) {
	return this->facesObservers.find(observer) != this->facesObservers.end();
}

void PersonMonitor::notify(const std::vector<Face>& faces) const {
	for(PersonFacesIterator iterator=facesObservers.begin(); iterator != facesObservers.end(); ++iterator){
		(*iterator)->onFacesDetected(faces);
	}
}



bool PersonMonitor::isReady() const {
	return this->personCapture != NULL && this->personCapture->isOpened() &&
			this->faceIdentifier != NULL &&	this->faceIdentifier->isReady()
		&& this->faceDetector != NULL && this->faceDetector->isReady();
}

cv::Mat PersonMonitor::prepareImage(const cv::Mat& img) const {
	Mat out;
	//FIXME: refatorar
	resize(img,out,Size(320,240 ));
	ImagePreprocessor::instance()->convertToGray(out,out);
	return out;
}
