/*
 * PersonFacesObserver.h
 *
 *  Created on: 04/06/2014
 *      Author: leobrizolara
 */

#ifndef PERSONFACESOBSERVER_H_
#define PERSONFACESOBSERVER_H_

#include "Face.h"
#include <vector>

class PersonFacesObserver {
public:
	virtual ~PersonFacesObserver(){}
	virtual void onFacesDetected(const std::vector<Face> & faces)=0;
};

#endif /* PERSONFACESOBSERVER_H_ */
