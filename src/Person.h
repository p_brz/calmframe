/*
 * Person.h
 *
 *  Created on: 27/04/2014
 *      Author: leobrizolara
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>

class Person {
private:
	int id;
	std::string name;
public:
	Person(int someId=-1);

	int getId() const;
	void setId(int id);
	const std::string& getName() const;
	void setName(const std::string& name);
};

#endif /* PERSON_H_ */
