/*
 * PersonRecognizer.h
 *
 *  Created on: 02/05/2014
 *      Author: leobrizolara
 */

#ifndef PERSONRECOGNIZER_H_
#define PERSONRECOGNIZER_H_

#include <vector>
#include <opencv2/core.hpp>
#include "Face.h"

class PersonRecognizer {
public:
	void setup();
	bool isReady() const;
	std::vector<Face> detectFaces(cv::InputArray img);
};

#endif /* PERSONRECOGNIZER_H_ */
