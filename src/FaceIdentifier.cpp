/*
 * FaceIdentifier.cpp
 *
 *  Created on: 20/04/2014
 *      Author: leobrizolara
 */

#include "FaceIdentifier.h"

#include <assert.h>

using namespace std;
using namespace cv;

#include "ImageComparator.h"

FaceIdentifier::FaceIdentifier()
	: ready(false)
	, useFaceValidation(true)
	, faceValidationThreshold(0.6)
	, recognizer(createEigenFaceRecognizer())
{
}

void FaceIdentifier::train(cv::InputArrayOfArrays src, cv::InputArray labels) {
	assert(!this->recognizer.empty());
	if(!this->recognizer.empty()){
		this->recognizer->train(src,labels);
		//TODO: checar se realmente recognizer foi treinado corretamente
		ready = true;
	}
}

int FaceIdentifier::identify(const cv::_InputArray& faceImg, double * outConfidence) const {
	assert(!this->recognizer.empty());

	if(useFaceValidation){
		Mat faceImgMat = faceImg.getMat();
		if(!this->isAKnownFace(faceImgMat)){
			return -1;
		}
	}

	if(outConfidence == NULL){
		return this->recognizer->predict(faceImg);
	}
	else{
		int predictedLabel = -1;
		double confidence = 0.0;
		this->recognizer->predict(faceImg, predictedLabel, confidence);

		*outConfidence = confidence;

		return predictedLabel;
	}
}
bool FaceIdentifier::isAKnownFace(const cv::Mat& srcImg, const Face& face)const {
	return this->isAKnownFace(srcImg(face.getRegion()));
}

Mat FaceIdentifier::reconstructFace(const cv::Mat& faceImg) const{
	//FIXME: testar se recognizer possui eigenvectors

	// Get some required data from the FaceRecognizer model.
	Mat eigenvectors = this->recognizer->get<Mat>("eigenvectors");
	Mat averageFaceRow = this->recognizer->get<Mat>("mean");


	// Project the input image onto the eigenspace.
	Mat projection = subspaceProject(eigenvectors, averageFaceRow, faceImg.reshape(1,1));
	// Generate the reconstructed face back from the eigenspace.
	Mat reconstructionRow = subspaceReconstruct(eigenvectors,
	averageFaceRow, projection);
	// Make it a rectangular shaped image instead of a single row.
//	Mat reconstructionMat = reconstructionRow.reshape(1, faceHeight);
	Mat reconstructionMat = reconstructionRow.reshape(1, faceImg.rows);
	// Convert the floating-point pixels to regular 8-bit uchar.
	Mat reconstructedFace = Mat(reconstructionMat.size(), CV_8U);
	reconstructionMat.convertTo(reconstructedFace, CV_8U, 1, 0);

	return reconstructedFace;
}



bool FaceIdentifier::isAKnownFace(const cv::Mat& faceImg) const{
	Mat reconstructedFace = this->reconstructFace(faceImg);

	ImageComparator comparator;
	double similarity = comparator.getSimilarity(faceImg,reconstructedFace);

	return similarity > faceValidationThreshold;
}
