/*
 * PersonMonitor.h
 *
 *  Created on: 19/05/2014
 *      Author: leobrizolara
 */

#ifndef PERSONMONITOR_H_
#define PERSONMONITOR_H_

#include <opencv2/highgui.hpp>
#include <set>
#include "Person.h"
#include "FaceIdentifier.h"
#include "PersonMonitorObserver.h"
#include "FaceDetector.h"
#include "FacePreprocessor.h"
#include "PersonFacesObserver.h"

/** Monitora uma câmera ou outra entrada de vídeo para detectar a aparição de alguém.*/
class PersonMonitor {
	cv::VideoCapture * personCapture;
	FaceIdentifier * faceIdentifier;
	FaceDetector * faceDetector;
	FacePreprocessor * facePreprocessor;

	std::set<PersonMonitorObserver *> observers;
	std::set<PersonFacesObserver *> facesObservers;
	typedef std::set<PersonMonitorObserver *>::iterator ObserversIterator;
	typedef std::set<PersonFacesObserver *>::iterator PersonFacesIterator;

public:
	PersonMonitor(cv::VideoCapture * capture=NULL
				, FaceIdentifier * faceIdentifier=NULL
				, FaceDetector * detector=NULL
				, FacePreprocessor * preprocessor = NULL);
	bool update();
	void registerObserver(PersonMonitorObserver * observer);
	void unregisterObserver(PersonMonitorObserver * observer);
	bool isRegistered(PersonMonitorObserver * observer);


	void registerObserver(PersonFacesObserver * observer);
	void unregisterObserver(PersonFacesObserver * observer);
	bool isRegistered(PersonFacesObserver * observer);

	bool isReady() const;
protected:
	void notify(const std::vector<Person>&  persons) const;
	void notify(const std::vector<Face>&  faces) const;
	cv::Mat prepareImage(const cv::Mat & img)const;
public: //getters and setters
	FaceIdentifier* getFaceIdentifier() const {
		return faceIdentifier;
	}

	void setFaceIdentifier(FaceIdentifier* faceIdentifier) {
		this->faceIdentifier = faceIdentifier;
	}

	cv::VideoCapture* getPersonCapture() const {
		return personCapture;
	}

	void setPersonCapture(cv::VideoCapture* personCapture) {
		this->personCapture = personCapture;
	}
};

#endif /* PERSONMONITOR_H_ */
