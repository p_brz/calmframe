/*
 * LocalPhotoDAO.h
 *
 *  Created on: 20/05/2014
 *      Author: leobrizolara
 */

#ifndef LOCALPHOTODAO_H_
#define LOCALPHOTODAO_H_

#include <vector>
#include "Photo.h"
#include "Person.h"
#include "PhotoDAO.h"
#include "LocalPhotoSource.h"

class LocalPhotoDAO : public PhotoDAO{
private:
	std::vector<Photo> photosCache;
	unsigned int lastPhotoId;
	static const int initialPhotoId;
public:
	LocalPhotoDAO()
		: lastPhotoId(initialPhotoId)
	{}
	LocalPhotoDAO(const std::string & rootPath);
	LocalPhotoDAO(LocalPhotoSource & source);

	std::vector<Photo> listAll() const;
	std::vector<Photo> findPhotosByPerson(const Person & person) const;
	void create(const Photo & photo);
	void create(const std::vector<Photo> & photo);
	void update(const Photo & photo);
	void update(const std::vector<Photo> & photo);
	void removeAll();
protected:
	int findPhoto(int photoId);
	void copyFromPhotoSource(LocalPhotoSource & photoSource);
};

#endif /* LOCALPHOTODAO_H_ */
