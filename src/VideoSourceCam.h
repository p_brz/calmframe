/*
 * VideoSourceCam.h
 *
 *  Created on: 30/05/2014
 *      Author: leobrizolara
 */

#ifndef VIDEOSOURCECAM_H_
#define VIDEOSOURCECAM_H_

#include "VideoSource.hpp"

#include <opencv2/core.hpp> //Mat
#include <opencv2/highgui.hpp> //VideoCapture

class VideoSourceCam : public VideoSource{
private:
	int camNumber;
	int frameCount;
	bool playing;
	int currentFPS;
	cv::VideoCapture video;
	cv::Mat currentFrame;
public:
	VideoSourceCam(int camNumber);
	bool start();
	void pause();
	void resume();
	void stop();
	bool update();

	bool isPlaying() const;

	int getFrameCount() const;
	int getVideoFPS() const;
	cv::Mat & getCurrentFrame();
//	const cv::VideoCapture & getVideo() const;
	cv::VideoCapture & getVideo();
};

#endif /* VIDEOSOURCECAM_H_ */
