/*
 * FaceTrainer.h
 *
 *  Created on: 18/05/2014
 *      Author: leobrizolara
 */

#ifndef FACETRAINER_H_
#define FACETRAINER_H_

#include <vector>
#include <string>
#include <opencv2/core.hpp>
#include "Face.h"
#include "FaceIdentifier.h"

class FaceTrainer {
private:
	std::vector<cv::Mat> preparedFaces;
	std::vector<int>  personIds;
public:
	void addFaceToTrain(const cv::Mat & img, const Face & face, int personId);
	void saveTrainedFaces(const std::string & directory) const;
	int getFacesToTrain(std::vector<cv::Mat> & preparedFaces, std::vector<int> & personIds) const;
	void train(FaceIdentifier & faceIdentifier);
};

#endif /* FACETRAINER_H_ */
