/*
 * Face.h
 *
 *  Created on: 23/04/2014
 *      Author: leobrizolara
 */

#ifndef FACE_H_
#define FACE_H_
#include <opencv2/core.hpp>

#include <vector>

class Face {
private:
	int personId;
	cv::Rect region;

	cv::Rect leftEye, rightEye;
public:
	static const int unknownPerson;
public:
	Face(const cv::Rect & faceRegion = cv::Rect(0,0,0,0));

	int getPersonId() const;
	const cv::Rect& getRegion() const;
	const cv::Rect& getLeftEye() const;
	const cv::Rect& getRightEye() const;
	std::vector<cv::Rect> getEyes() const;

	void setPersonId(int peopleId);
	void setRegion(const cv::Rect& region);
	void setLeftEye(const cv::Rect& leftEye);
	void setRightEye(const cv::Rect& rightEye);

	bool hasRightEye() const;
	bool hasLeftEye() const;
};

#endif /* FACE_H_ */
