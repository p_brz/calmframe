/*
 * LocalPhotoSource.cpp
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#include <LocalPhotoSource.h>

#include <assert.h>
#include "Poco/File.h"
#include "Poco/Path.h"
#include <set>
using namespace std;
using namespace Poco;

const std::string& LocalPhotoSource::getRootPath() const {
	return rootPath;
}

void LocalPhotoSource::setRootPath(const std::string& rootPath) {
	this->rootPath = rootPath;
}

LocalPhotoSource::LocalPhotoSource(const std::string& photoRootPath)
	: currentPhotoIndex(0u)
	, rootPath(photoRootPath)
{
}

bool LocalPhotoSource::loadPhotos(bool recursiveSearch) {

	this->photos.clear();
	if(recursiveSearch){
		return this->loadPhotosRecursive(this->rootPath, this->photos);
	}
	else{//FIXME: unificar busca recursiva e esta
		File rootDir = File(this->rootPath);
		if(rootDir.exists() && rootDir.isDirectory()){

			rootDir.list(this->photos);
			this->filterPhotoFiles(this->photos);
			return true;
		}
		return false;
	}
}

//void printSubdir(const string & rootFile,
//		const vector<string> & subDirectories)
//{
//	cout << "SubDirectories of " << rootFile <<":"<<endl;
//	for(size_t i=0; i < subDirectories.size(); ++i){
//		cout << subDirectories[i] <<endl;
//	}
//	cout << "END SubDirectories" << endl<<endl;
//}
//void printPhotoFiles(const string & rootFile,
//		const vector<string> & photoFiles)
//{
//	cout << "Photo photoFiles in " << rootFile <<":"<<endl;
//	for(size_t i=0; i < photoFiles.size(); ++i){
//		cout << photoFiles[i] <<endl;
//	}
//	cout << "END Photo photoFiles" << endl<<endl;
//}

//TODO: modularizar método (reduzir complexidade)
//TODO: otimizar (reduzir conversões Path to string)
bool LocalPhotoSource::loadPhotosRecursive(
		const std::string& rootFile
		, std::vector<std::string>& files)
{
	File rootDir = File(rootFile);
	if(rootDir.exists() && rootDir.isDirectory()){
		rootDir.list(files);

		vector<string> subDirectories = vector<string>(files);
		this->filterDirectories(subDirectories, rootFile);
		this->filterPhotoFiles(files);

//		printSubdir(rootFile,subDirectories);
//		printPhotoFiles(rootFile, files);

		bool loadOk = true;
		for(size_t i =0; i < subDirectories.size(); ++i){
			Path subdir = Path(rootFile,subDirectories[i]);
			vector<string> subFiles;
			loadOk = loadOk && this->loadPhotosRecursive(subdir.toString(), subFiles);

			for(size_t subFileIdx =0; subFileIdx < subFiles.size(); ++subFileIdx){
				string subFileName = subDirectories[i] + Path::separator() + subFiles[subFileIdx];
				files.push_back(subFileName );
			}
		}
		return loadOk;
	}
	return false;
}

unsigned int LocalPhotoSource::photoCount() const {
	return this->photos.size();
}

unsigned int LocalPhotoSource::getCurrentPhotoNumber() const {
	return this->currentPhotoIndex;
}

void LocalPhotoSource::nextPhoto() {
	this->currentPhotoIndex = (currentPhotoIndex + 1)%this->photoCount();
}

void LocalPhotoSource::previousPhoto() {
	this->currentPhotoIndex = (currentPhotoIndex - 1 + this->photoCount())%this->photoCount();
}

void LocalPhotoSource::setCurrentPhoto(unsigned int photoNumber) {
	assert(photoNumber >=0u && photoNumber < this->photoCount());

	this->currentPhotoIndex = photoNumber;
}

std::string LocalPhotoSource::getCurrentPhotoPath() const {
	assert(this->photoCount() > 0);
	return this->rootPath + Path::separator() + this->photos[this->currentPhotoIndex];
}

std::string LocalPhotoSource::getPhotoPath(unsigned int photoNumber) const {
	assert(this->photoCount() > 0 && photoNumber >= 0 && photoNumber < this->photoCount());

	return this->rootPath + Path::separator() + this->photos[photoNumber];
}

std::string LocalPhotoSource::getCurrentPhotoAbsolutePath(
		const std::string& basePath) const {
	Path path = Path(this->getCurrentPhotoPath());
	if(basePath.length() > 0){
		path = path.makeAbsolute(basePath);
	}
	else{
		path = path.makeAbsolute();
	}
	return path.toString();
}
void LocalPhotoSource::filterDirectories(std::vector<std::string>& files, const std::string & basePath ) const {
	for(size_t i=0; i < files.size(); ++i){
		File file = Path(basePath,files[i]);

		//Se não e é diretório
		if(!file.isDirectory())
		{
			//Elimina arquivo
			files.erase(files.begin() + i);
			--i; //removeu um arquivo
		}
	}
}

void LocalPhotoSource::filterPhotoFiles(std::vector<std::string> & files) const {
	//FIXME: filtragem temporária, buscar método mais robusto (talvez com libmagic)
	static const char * allowedExtensions[] = {"jpg", "jpeg", "png", "tiff", "pgm", "gif"};
	static set<string> extensions = set<string>(allowedExtensions, allowedExtensions + 6);

	for(size_t i=0; i < files.size(); ++i){
		Path file = Path(files[i]);
		//Se é diretório ou extensão não é válida
		if(file.isDirectory() ||
				extensions.find(file.getExtension()) == extensions.end())
		{
			//Elimina arquivo
			files.erase(files.begin() + i);
			--i; //removeu um arquivo
		}
	}
}
