/*
 * StringUtils.cpp
 *
 *  Created on: 20/04/2014
 *      Author: leobrizolara
 */

#include "StringUtils.h"

#include <sstream>
using namespace std;

template<typename Number>
string numberToString(Number number){
	ostringstream numStr;
	numStr << number;
	return numStr.str();
}

std::string StringUtils::toString(long number) {
	return numberToString<long>(number);
}
std::string StringUtils::toString(unsigned long number) {
	return numberToString<unsigned long>(number);
}

std::string StringUtils::toString(double number) {
	return numberToString<double>(number);
}

std::string StringUtils::toString(int number) {
	return numberToString<int>(number);
}
std::string StringUtils::toString(unsigned int number) {
	return numberToString<unsigned int>(number);
}

long StringUtils::toLong(const std::string& str) {
	long result;
	if ( !(stringstream(str) >> result) ){
	    result = 0;//if that fails set result to 0
	}
	return result;
}
