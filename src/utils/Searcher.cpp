/*
 * ContainerSearcher.cpp
 *
 *  Created on: 26/05/2014
 *      Author: leobrizolara
 */

#include <utils/Searcher.h>

const Searcher* Searcher::instance() {
	static Searcher singleton;
	return &singleton;
}
