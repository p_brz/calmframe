/*
 * CsvRow.cpp
 *
 *  Created on: 29/04/2014
 *      Author: leobrizolara
 */

#include <utils/CsvRow.h>
#include <assert.h>

using namespace std;

//TODO: (?) mover funcionalidade para uma classe ou arquivo
static void splitString(const string & input, const char separator, vector<string> & out){
	if(input.length() > 0){
		size_t previousPos = 0;
		size_t currentPos;

		do{
			currentPos = input.find(separator,previousPos);

			string splittedString = (currentPos != string::npos
										? input.substr(previousPos, currentPos - previousPos)
										: input.substr(previousPos, input.size() - previousPos));

			out.push_back(splittedString);
			previousPos = currentPos + 1;
		}while(currentPos != string::npos);
	}
}



CsvRow::CsvRow()
{
}
CsvRow::CsvRow(const std::string& csvLine, char separator) {
	this->setRow(csvLine, separator);
}

CsvRow::CsvRow(const std::vector<std::string>& csvCollumns) {
	this->columns = csvCollumns;
}

void CsvRow::setRow(const std::string& csvLine, char separator) {
	this->columns.clear();
	splitString(csvLine, separator, this->columns);
}

int CsvRow::getColumnCount() const {
	return this->columns.size();
}
//#include <iostream>
std::string CsvRow::get(unsigned int index) const{
//	if(!(index < this->columns.size())){
//		std::cerr << "Error" <<std::endl;
//		for(size_t i=0; i < this->columns.size(); ++i){
//			cout << this->columns[i]<< endl;
//		}
//	}
	assert(index < this->columns.size());
	if(index < this->columns.size()){
		return this->columns[index];
	}
	return string();
}

bool CsvRow::isEmpty() const {
	return this->columns.empty();
}

void CsvRow::clear() {
	this->columns.clear();
}
