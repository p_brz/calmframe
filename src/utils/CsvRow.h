/*
 * CsvRow.h
 *
 *  Created on: 29/04/2014
 *      Author: leobrizolara
 */

#ifndef CSVROW_H_
#define CSVROW_H_

#include <string>
#include <vector>

class CsvRow {
private:
	std::vector<std::string> columns;
public:
	CsvRow();
	CsvRow(const std::string & csvLine, char separator=',');
	CsvRow(const std::vector<std::string> & csvCollumns);

	int getColumnCount() const;
	std::string get(unsigned int i) const;
	bool isEmpty() const;
	void setRow(const std::string & csvLine, char separator=',');
	void clear();
};

#endif /* CSVROW_H_ */
