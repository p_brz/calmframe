/*
 * CsvReader.h
 *
 *  Created on: 28/04/2014
 *      Author: leobrizolara
 */

#ifndef CSVREADER_H_
#define CSVREADER_H_

#include <string>
#include <vector>
#include <fstream>

#include "CsvRow.h"

class CsvReader {
	//TODO: CsvReader poderia retornar forward-iterator para o arquivo
private:
	int rowIndex;
	std::ifstream csvFile;
	CsvRow currentRow;
	char separator;
public:
	CsvReader(char columnSeparator=',');
//	virtual ~CsvReader();

	void open(const std::string & filename);
	bool isOpen() const;
	void close();
	bool nextRow(CsvRow * row = NULL);
	int getRowIndex() const;
	const CsvRow & getCurrentRow() const;
};

#endif /* CSVREADER_H_ */
