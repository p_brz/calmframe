/*
 * CsvReader.cpp
 *
 *  Created on: 28/04/2014
 *      Author: leobrizolara
 */

#include "utils/CsvReader.h"

#include <string>
using namespace std;


//static bool read_csv(const string& filename, vector & images, vector<int>& labels, char separator = ';') {
//    std::ifstream file(filename.c_str(), ifstream::in);
//    if (!file) {
//        return false;
//    }
//    string line, path, classlabel;
//    while (getline(file, line)) {
//        stringstream liness(line);
//        getline(liness, path, separator);
//        getline(liness, classlabel);
//
//
//    }
//	return true;
//}

CsvReader::CsvReader(char columnSeparator)
	: rowIndex(-1)
	, separator(columnSeparator)
{
	// TODO Auto-generated constructor stub

}

//CsvReader::~CsvReader() {
//	if(this->isOpen()){
//		this->csvFile.close();
//	}
//}


void CsvReader::open(const std::string& filename) {
	this->csvFile.open(filename.c_str());
}

void CsvReader::close() {
	if(this->isOpen()){
		this->csvFile.close();
	}
	this->rowIndex = -1;
	this->currentRow.clear();
}

bool CsvReader::isOpen() const {
	return this->csvFile.is_open();
}

bool CsvReader::nextRow(CsvRow* row) {
	if(this->isOpen() && csvFile.good()){
		string line;
		std::getline(this->csvFile, line);
		if(csvFile.good() || csvFile.eof()){
			this->currentRow = CsvRow(line,this->separator);

			if(row != NULL){
				*row = this->currentRow;
			}

			++this->rowIndex;
			return csvFile.eof() ? false : true;
		}
	}
	return false;
}

int CsvReader::getRowIndex() const {
	return this->rowIndex;
}

const CsvRow& CsvReader::getCurrentRow() const {
	return this->currentRow;
}
