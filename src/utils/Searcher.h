/*
 * Searcher.h
 *
 *  Created on: 26/05/2014
 *      Author: leobrizolara
 */

#ifndef SEARCHER_H_
#define SEARCHER_H_
#include <vector>

class Searcher {
public:
	static const Searcher * instance();
public:

	template<typename T>
	int findElement(const std::vector<T> & container, const T & element, std::size_t startIndex=0) const;
	template<typename T>
	bool existElement(const std::vector<T> & container, const T & element, std::size_t startIndex=0) const;
};

template<typename T>
inline int Searcher::findElement(const std::vector<T>& container,
		const T& element, std::size_t startIndex) const
{
	for(std::size_t i=startIndex; i < container.size(); ++i){
		if(container[i] == element){
			return i;
		}
	}
	return -1;
}

template<typename T>
inline bool Searcher::existElement(const std::vector<T>& container,
		const T& element, std::size_t startIndex) const
{
	//Se índice retornado por findElement é válido, então o elemento existe
	return findElement(container,element,startIndex) >= 0;
}

#endif /* CONTAINERSEARCHER_H_ */
