/*
 * GeomUtils.h
 *
 *  Created on: 03/05/2014
 *      Author: leobrizolara
 */

#ifndef GEOMUTILS_H_
#define GEOMUTILS_H_

#include <opencv2/core.hpp>
#include <vector>
#include <cmath>

class GeomUtils {
public:
	static GeomUtils * instance();

	static float distance(const cv::Point2f & pt0, const cv::Point2f & pt1);

	template<typename T>
	static float distance(const cv::Point_<T> & pt0, const cv::Point_<T> & pt1);
	template<typename T>
	static cv::Point_<T> center(const cv::Rect & rect);
	template<typename T>
	static cv::Point_<T> center(const cv::Point_<T> & pt0, const cv::Point_<T> & pt1);
	static cv::Point2f center(const cv::Rect & rect);
	static cv::Point2f center(const cv::Point & pt0, const cv::Point & pt1);
	template<typename T>
	static double degreesBetween(const cv::Point_<T> & pt0, const cv::Point_<T> & pt1);

	/**
	 * Dos retângulos em objs, escolhe aquele cujo centro mais se aproxima do centro
	 * de region
	 * */
	static cv::Rect selectMoreCenteredObject(const cv::Rect& region
			, const std::vector<cv::Rect> & objs);
};

template<typename T>
inline cv::Point_<T> GeomUtils::center(const cv::Rect& rect) {
	cv::Point_<T> regionCenter = rect.tl();
	regionCenter.x += rect.width/2;
	regionCenter.y += rect.height/2;

	return regionCenter;
}

template<typename T>
inline cv::Point_<T> GeomUtils::center(const cv::Point_<T>& pt0,
		const cv::Point_<T>& pt1) {
	cv::Point_<T> center;
	center.x = (pt0.x + pt1.x)/2;
	center.y = (pt0.y + pt1.y)/2;
	return center;
}

template<typename T>
inline double GeomUtils::degreesBetween(const cv::Point_<T>& pt0,
		const cv::Point_<T>& pt1) {
	// Get the angle between the 2 eyes.
	double dy = (pt1.y - pt0.y );
	double dx = (pt1.x  - pt0.x);
	// Convert Radians to Degrees.
	double angle = atan2(dy, dx) * 180.0/CV_PI;

	return angle;
}

template<typename T>
inline float GeomUtils::distance(const cv::Point_<T>& pt0,
		const cv::Point_<T>& pt1) {

	const float deltaX = (pt0.x - pt1.x);
	const float deltaY = (pt0.y - pt1.y);
	return sqrtf(deltaX*deltaX + deltaY*deltaY);
}

#endif /* GEOMUTILS_H_ */
