/*
 * GeomUtils.cpp
 *
 *  Created on: 03/05/2014
 *      Author: leobrizolara
 */

#include <utils/GeomUtils.h>
#include <assert.h>

using namespace cv;
using namespace std;



GeomUtils* GeomUtils::instance() {
	static GeomUtils singleton;

	return &singleton;
}


//// Get the center between the 2 eyes.
//Point2f eyesCenter;
//eyesCenter.x = (lEyeCenter.x + rEyeCenter.x) * 0.5f;
//eyesCenter.y = (lEyeCenter.y + rEyeCenter.y) * 0.5f;
//// Get the angle between the 2 eyes.
//double dy = (rEyeCenter.y - lEyeCenter.y );
//double dx = (rEyeCenter.x  - lEyeCenter.x);
//double len = sqrt(dx*dx + dy*dy);
//// Convert Radians to Degrees.
//double angle = atan2(dy, dx) * 180.0/CV_PI;


cv::Point2f GeomUtils::center(const cv::Point& pt0, const cv::Point& pt1) {
	return GeomUtils::center<float>(pt0,pt1);
}
cv::Point2f GeomUtils::center(const cv::Rect& rect) {
	return GeomUtils::center<float>(rect);
}
//float GeomUtils::distance(const cv::Point2f& pt0, const cv::Point2f& pt1) {
//	const float deltaX = (pt0.x - pt1.x);
//	const float deltaY = (pt0.y - pt1.y);
//	return sqrtf(deltaX*deltaX + deltaY*deltaY);
//}
float GeomUtils::distance(const cv::Point2f& pt0, const cv::Point2f& pt1) {
	return GeomUtils::distance<float>(pt0,pt1);
}

cv::Rect GeomUtils::selectMoreCenteredObject(const cv::Rect& region,
		const std::vector<cv::Rect>& objects) {
	assert(objects.size() > 0);

	Rect selectedObject = objects[0];

	if(objects.size() > 1){
		Point2f regionCenter = center(region);
		Point objCenter = center(selectedObject);

		float currentDistance = distance(objCenter, regionCenter);

		for(size_t i =1; i < objects.size(); ++i){
			objCenter = center(objects[i]);
			float dist = distance(objCenter, regionCenter);
			if(dist < currentDistance){
				currentDistance = dist;
				selectedObject = objects[i];
			}
		}
	}

	return selectedObject;
}
