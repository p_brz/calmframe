/*
 * StringUtils.h
 *
 *  Created on: 20/04/2014
 *      Author: leobrizolara
 */

#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <string>

class StringUtils {
public:
	static std::string toString(int number);
	static std::string toString(unsigned int number);
	static std::string toString(long number);
	static std::string toString(unsigned long number);
	static std::string toString(double number);
	static long toLong(const std::string & str);
	template<typename Number>
	static Number toNumber(const std::string & str){
		return (Number)toLong(str);
	}
};

#endif /* STRINGUTILS_H_ */
