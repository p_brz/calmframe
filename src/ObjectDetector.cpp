/*
 * ObjectDetector.cpp
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#include "ObjectDetector.h"


#include <assert.h>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>

using namespace cv;
using namespace std;

ObjectDetector::ObjectDetector(const std::string& cascadeFilename,
		int flags,
        cv::Size minSize, cv::Size maxSize,
        double scaleFactor, int minNeighbors)
: ready(false)
, cascadeClassifier(new CascadeClassifier())
, detectionFlags(flags)
, detectionMinSize(minSize)
, detectionMaxSize(maxSize)
, detectionScaleFactor(scaleFactor)
, detectionMinNeighbors(minNeighbors)
{
	if(cascadeFilename.length() > 0){
		this->setup(cascadeFilename);
	}
}

bool ObjectDetector::setup(const std::string& cascadeFilename) {
	ready = this->cascadeClassifier->load(cascadeFilename);
	return ready;
}

bool ObjectDetector::isReady() const {
	return ready;
}

int ObjectDetector::detectObjects(const cv::_InputArray& image,
		std::vector<cv::Rect>& objects, cv::Mat* processedImage) const
{
	return this->detectObjectsImpl(image, objects, this->detectionFlags, processedImage);
}

int ObjectDetector::detectObjects(const cv::_InputArray& srcImage,
		std::vector<cv::Mat>& objImgs, bool grayscaleImgs, bool copyImages) const
{
	vector<Rect> objects;
	Mat processedImage;
	int detectedObject = this->detectObjectsImpl(srcImage, objects, this->detectionFlags, &processedImage);

	const Mat & imageToCrop = (grayscaleImgs ? processedImage : srcImage.getMat());
	for(unsigned int i=0; i < objects.size(); ++i){
		Mat cropedImage = Mat(imageToCrop, objects[i]);
		if(copyImages){
			cropedImage = cropedImage.clone();
		}
		objImgs.push_back(cropedImage);
	}

	return detectedObject;
}

int ObjectDetector::detectObjectsImpl(const cv::_InputArray& image,
		std::vector<cv::Rect>& objects, cv::Mat* processedImage) const
{
	return detectObjectsImpl(image,objects,this->detectionFlags,processedImage);
}
int ObjectDetector::detectObjectsImpl(const cv::_InputArray& image,
		std::vector<cv::Rect>& objects, int flags, cv::Mat* processedImage) const
{
	unsigned int initialSize = objects.size();
	Mat preparedImage = prepareImage(image);
	if(processedImage != NULL){
		*processedImage = preparedImage;
	}

	assert(preparedImage.cols > 0 && preparedImage.rows > 0);

	vector<Rect> tempObjects;
	cascadeClassifier->detectMultiScale(
			preparedImage
			, tempObjects
			, detectionScaleFactor, detectionMinNeighbors, flags
			, detectionMinSize, detectionMaxSize );

	correctBorderObjects(image, tempObjects);
	objects = tempObjects;

	assert(objects.size() >= initialSize);

	return objects.size() - initialSize;
}

cv::Mat ObjectDetector::prepareImage(const cv::_InputArray& image)  const{
	  Mat preparedImage;
	  //Converte imagem para GRAY
//	  image.convertTo(preparedImage, CV_8UC1); //parece que essa função não converte as cores

	  if(image.channels() > 1){
		  //nada garante que image esteja no formato BGR (apesar deste ser o padrão)
		  cv::cvtColor(image, preparedImage, cv::COLOR_BGR2GRAY);
		  equalizeHist( preparedImage, preparedImage );
	  }
	  else{
		  preparedImage = image.getMat();
	  }

	  return preparedImage;
}

int ObjectDetector::detectLargestObject(const cv::_InputArray& image,
		cv::Rect& largestObject, cv::Mat* processedImage) const
{
	int flags = this->detectionFlags | CASCADE_FIND_BIGGEST_OBJECT;

	std::vector<Rect>objects = std::vector<Rect>();

	int detectedObjects = this->detectObjectsImpl(image,objects,flags,processedImage);
	if(detectedObjects > 0){
		largestObject = objects[0];
		return 1;
	}
	else{
		return 0;
	}
}

int ObjectDetector::detectLargestObject(const cv::_InputArray& image, cv::Mat& objImg,
		bool grayscaleImgs, bool copyImages) const
{
	Rect obj;
	Mat processedImage;
	if(this->detectLargestObject(image, obj,&processedImage) > 0){
		Mat outImg = Mat((grayscaleImgs ? processedImage : image.getMat()), obj);
		objImg = (copyImages ? objImg.clone() : outImg);
		return 1;
	}
	return 0;
}

void ObjectDetector::correctBorderObjects(const cv::_InputArray& img, std::vector<cv::Rect>& objects) const{
	for(size_t i=0; i < objects.size(); ++i){
		if (objects[i].x < 0){
			objects[i].x = 0;
		}
		if (objects[i].y < 0){
			objects[i].y = 0;
		}
//		if (objects[i].x + objects[i].width > img.cols){
//			objects[i].x = img.cols - objects[i].width;
//		}
//		if (objects[i].y + objects[i].height > img.rows){
//			objects[i].y = img.rows - objects[i].height;
//		}
		if (objects[i].x + objects[i].width > img.cols()){
			objects[i].x = img.cols() - objects[i].width;
		}
		if (objects[i].y + objects[i].height > img.rows()){
			objects[i].y = img.rows() - objects[i].height;
		}
	}
}
