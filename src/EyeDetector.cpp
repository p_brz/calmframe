/*
 * EyeDetector.cpp
 *
 *  Created on: 23/04/2014
 *      Author: leobrizolara
 */

#include "EyeDetector.h"

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <math.h>
#include <assert.h>

#include "utils/GeomUtils.h"

using namespace cv;
using namespace std;

double EyeDetector::getSearchHeight() const {
	return searchHeight;
}

void EyeDetector::setSearchHeight(double searchHeight) {
	this->searchHeight = searchHeight;
}

double EyeDetector::getSearchOffsetX() const {
	return searchOffsetX;
}

void EyeDetector::setSearchOffsetX(double searchOffsetX) {
	this->searchOffsetX = searchOffsetX;
}

double EyeDetector::getSearchOffsetY() const {
	return searchOffsetY;
}

void EyeDetector::setSearchOffsetY(double searchOffsetY) {
	this->searchOffsetY = searchOffsetY;
}

double EyeDetector::getSearchWidth() const {
	return searchWidth;
}

void EyeDetector::setSearchWidth(double searchWidth) {
	this->searchWidth = searchWidth;
}



//#define EYE_SX 0.16
#define EYE_SX 0.10
#define EYE_SY 0.26
//#define EYE_SW 0.30
#define EYE_SW 0.35
#define EYE_SH 0.28
EyeDetector::EyeDetector(const std::string& eyesCascadeFilename)
: searchOffsetX(EYE_SX)
, searchOffsetY(EYE_SY)
, searchWidth(EYE_SW)
, searchHeight(EYE_SH)
, eyeDetector(eyesCascadeFilename)
{
}

bool EyeDetector::setup(const std::string& eyesCascadeFilename) {
	this->eyeDetector.setup(eyesCascadeFilename);
	return isReady();
}

bool EyeDetector::isReady() const {
	return eyeDetector.isReady();
}

//deprecated
int EyeDetector::detectEyesCandidates(const cv::Mat& srcImage, const cv::Rect& facePoints,
		std::vector<cv::Rect>& eyes) const{
	return 0;
}


int EyeDetector::detectEyes(const cv::Mat& srcImage, Face& face) const {
	Rect left, right;
	int detected = detectEyes(srcImage,face.getRegion(),left,right);
	face.setLeftEye(left);
	face.setRightEye(right);

	return detected;
}

int EyeDetector::detectEyes(const cv::Mat& srcImage, const cv::Rect& facePoints,
		cv::Rect& leftEye, cv::Rect& rightEye) const
{
	Rect lRegion = Rect(0,0,0,0), rRegion = Rect(0,0,0,0);
	extractEyesRegion(facePoints, lRegion, rRegion);
	
	int countDetection =0;
	//FIXME: utilizar teste p/ tamanho minimo em lRegion e rRegion da imagem (em vez de utilizar tamanho da face)
	if(detectEye(leftEye,srcImage, lRegion, facePoints.size())){
		++countDetection;
	}
	else{
		leftEye = Rect(0,0,0,0);
	}

	//FIXME: utilizar teste p/ tamanho minimo em lRegion e rRegion da imagem (em vez de utilizar tamanho da face)
	if(detectEye(rightEye,srcImage, rRegion, facePoints.size())){
		++countDetection;
	}
	else{
		rightEye = Rect(0,0,0,0);
	}
	
	return countDetection;
}

static bool imageContainsRegion(const cv::Mat& srcImage, const cv::Rect& region){
	return (region.x >= 0)
			&& (region.width  > 0)
			&& (region.x + region.width <= srcImage.cols)
			&& (region.y >= 0)
			&& (region.height  > 0)
			&& (region.y + region.height <= srcImage.rows);
}
static bool regionContainsRegion(const cv::Rect& topRegion, const cv::Rect& region){
	return (region.x >= topRegion.x)
			&& (region.width  > 0)
			&& (region.x + region.width <= topRegion.x + topRegion.width)
			&& (region.y >= topRegion.y)
			&& (region.height  > 0)
			&& (region.y + region.height <= topRegion.y + topRegion.height);
}

bool EyeDetector::detectEye(cv::Rect& eye, const cv::Mat& srcImage,
		const cv::Rect& eyeSchReg, const cv::Size& schRegSize) const
{
	assert(imageContainsRegion(srcImage, eyeSchReg));
//	Mat eyeROI = srcImage(eyeSchReg).clone();
	Mat eyeROI = srcImage(eyeSchReg);
	resize(eyeROI, eyeROI, schRegSize);

	vector<Rect> eyesTmp;
	if(eyeDetector.detectObjects(eyeROI,eyesTmp) > 0){

		eye = GeomUtils::selectMoreCenteredObject(eyeSchReg, eyesTmp) ;

		double largeWidth = schRegSize.width;
		double largeHeight = schRegSize.height;
		int x = (eye.x / largeWidth) * eyeSchReg.width;
		int y = (eye.y / largeHeight) * eyeSchReg.height;
		int width = (eye.width / largeWidth) * eyeSchReg.width;
		int height = (eye.height / largeHeight) * eyeSchReg.height;

		eye = Rect(x,y,width,height) + eyeSchReg.tl();

		//Eye deve estar contido na imagem de origem
		assert(imageContainsRegion(srcImage,eye));
		assert(regionContainsRegion(eyeSchReg, eye));

		return true;
	}

	return false;
}



void EyeDetector::extractEyesRegion(const cv::Rect& facePoints, cv::Rect& leftEyeRegion,
		cv::Rect& rightEyeRegion) const
{
	int leftX      = cvRound(facePoints.width  * searchOffsetX);
	int topY       = cvRound(facePoints.height * searchOffsetY);
	int widthX     = cvRound(facePoints.width  * searchWidth);
	int heightY    = cvRound(facePoints.height * searchHeight);
	int rightX 	   = cvRound(facePoints.width  * (1.0- searchOffsetX - searchWidth));
	leftEyeRegion  = Rect(leftX,  topY, widthX, heightY) + facePoints.tl();
	rightEyeRegion = Rect(rightX, topY, widthX, heightY) + facePoints.tl();
}
