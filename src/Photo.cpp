/*
 * Photo.cpp
 *
 *  Created on: 15/05/2014
 *      Author: leobrizolara
 */

#include "Photo.h"

#include <assert.h>
using namespace std;

const int Photo::invalidPhotoId = -1;
const int Photo::unknownFaceNumber = -1;

Photo::Photo(const std::string& imgPath)
	: id(Photo::invalidPhotoId)
	, imagePath(imgPath)
	, facesCount(unknownFaceNumber)
{
}

void Photo::setImagePath(const std::string& imgPath){
	this->imagePath = imgPath;
}

const std::string & Photo::getImagePath() const {
	return this->imagePath;
}

void Photo::setFaces(const std::vector<Face>& faces) {
	this->photoFaces = faces;
	this->facesCount = faces.size();
}


const std::vector<Face> & Photo::getFaces() const {
	return this->photoFaces;
}

int Photo::getFacesCount() const {
	return this->facesCount;
}

const Face & Photo::getFace(unsigned int index) const {
	assert(index >=0 && index <this->photoFaces.size());
	if(index >=0 && index < this->photoFaces.size()){
		return this->photoFaces[index];
	}
	return photoFaces[0];
}

int Photo::getId() const {
	return id;
}

void Photo::setFacesToZero() {
	this->facesCount=0;
	this->photoFaces.clear();
}

void Photo::setFacesToUnknown() {
	this->facesCount=unknownFaceNumber;
	this->photoFaces.clear();
}

std::set<int> Photo::getPersonsId() const {
	set<int> personsId;
	for(size_t i=0; i < photoFaces.size(); ++i){
//		if(photoFaces[i].getPersonId() != Face::unknownPerson){
//			personsId.push_back(photoFaces[i].getPersonId());
//		}
		personsId.insert(photoFaces[i].getPersonId());
	}
	return personsId;
}

void Photo::setId(int id) {
	this->id = id;
}
