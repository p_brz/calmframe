/*
 * eyeDetection.cpp
 *
 *  Created on: 23/04/2014
 *      Author: leobrizolara
 */

#include <opencv2/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui.hpp>
#include <iostream>
using namespace cv;
using namespace std;

#include <stdexcept>
#include <assert.h>
#include "FaceDetector.h"
#include "VideoPlayer.h"
#include "Face.h"

const char * defaultCascadeFilename = "./data/lbpcascade_frontalface.xml";
const char * defaultEyeCascadeFilename = "./data/haarcascade_eye.xml";
//const char * VideoFaceDetectorApp::defaultSourceVideo = "./data/Marilha_Cam1.wmv";
const char * defaultSourceVideo = "/media/FILES/LEO/VIDEOS/PrideAndPrejudice/pride_and_prejudice.avi";
const char * windowName = "video";

int delay=25;
int backupDelay=25;

FaceDetector detector;
VideoPlayer player;
vector<Face> globalFaces;

#include "eyeDetection.h"

bool setup(int argc, char * argv[]) {
	if(!detector.setup(defaultCascadeFilename)){
		cerr << "Could not setup face loader with file: " << defaultCascadeFilename << endl;
		return false;
	}
	if(!detector.setupEyesDetector(defaultEyeCascadeFilename)){
		cerr << "Could not setup face loader eye detector with file: " << defaultEyeCascadeFilename << endl;
		return false;
	}

    const char * sourceVideo = (argc < 2 ? defaultSourceVideo : argv[1]);
    namedWindow(windowName);
    if (!player.start(sourceVideo))
    {
        cerr  << "Could not open video " << sourceVideo << endl;
        return false;
    }
    player.enableTrackbar(windowName);

//    int videoFrameWidth = (int) player.getVideo().get(CAP_PROP_FRAME_WIDTH);
//    int videoFrameHeight = (int) player.getVideo().get(CAP_PROP_FRAME_HEIGHT);


    delay = 1000/player.getVideoFPS();
    backupDelay = delay;

	return true;
}

//void detectFaces(const Mat & frame, vector<Face> & faces){
//	assert(faces.size() == 0);
//	vector<Rect> rects;
//	detector.detectFaces(frame,rects);
//
//	for(size_t i=0; i < rects.size(); ++i){
//		Face face;
//		face.setRegion(rects[i]);
//
//		const Rect & faceRegion = face.getRegion();
//		Rect searchRegion = Rect(faceRegion.x,faceRegion.y
//						,faceRegion.width, faceRegion.height);
//		searchRegion.height = (searchRegion.height > 1 ? (int)(searchRegion.height*0.6) : 1);
//
//		vector<Rect> eyes;
//		if (detector.detectEyes(frame,  searchRegion, eyes) < 0 ){
//			cerr << "Error detecting eyes in frame " << player.getFramePosition() << endl;
//		}
//		face.setEyes(eyes);
//
//		faces.push_back(face);
//	}
//}

void drawFaces(Mat & frame, vector<Face> & faces){
	for(size_t i=0; i < faces.size(); ++i){

		Face & currentFace = faces[i];
		rectangle(frame, currentFace.getRegion(),Scalar(255,0,0));
		currentFace.setLeftEye(Rect(0,0,0,0));
		currentFace.setRightEye(Rect(0,0,0,0));


		for(size_t eyesIdx=0; eyesIdx < currentFace.getEyes().size(); ++eyesIdx){

			Scalar col = Scalar(0,255,0);
			Rect r =  currentFace.getEyes()[eyesIdx] + currentFace.getRegion().tl();
			rectangle(frame, r, col);
		}
	}
}

bool update() {
	if(!player.update()){
		return false; //encerra
	}
	Mat & frame = player.getCurrentFrame();

//	//Reduzir tamanho do frame aumenta velocidade, mas diminui precisão
//	resize(frame,frame, Size((frame.cols*3)/4,(frame.rows*3)/4));

	if(player.getFrameCount() % 5 == 0){
		globalFaces.clear();
		detector.detectFaces(frame,globalFaces);
		delay = 1;
	}
	else{
		delay = backupDelay;
	}

	drawFaces(frame,globalFaces);

	////////////////////////////////// Show Image /////////////////////////////////////////////
	imshow(windowName, frame);
	return true;
}

int mainEyeDetection(int argc, char * argv[]){
	if(setup(argc, argv)){
		bool continueLoop = true;
		do{
			update();
			int key = waitKey(delay);
			if(key != -1){
				if(((char)key) == 27)
					continueLoop = false;
			}
		}while(continueLoop);
	}
	return 0;
}
