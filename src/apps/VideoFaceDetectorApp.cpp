/*
 * VideoFaceDetectorApp.cpp
 *
 *  Created on: 16/04/2014
 *      Author: leobrizolara
 */

#include "VideoFaceDetectorApp.h"

#include <opencv2/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui.hpp>
#include <iostream>
using namespace cv;
using namespace std;

#include <stdexcept>
#include <assert.h>

const int VideoFaceDetectorApp::KEY_ESC = 27;

const char * VideoFaceDetectorApp::defaultCascadeFilename = "./data/lbpcascade_frontalface.xml";
//const char * VideoFaceDetectorApp::defaultEyeCascadeFilename = "./data/haarcascade_eye.xml";
const char * VideoFaceDetectorApp::defaultEyeCascadeFilename = "./data/haarcascade_eye_tree_eyeglasses.xml";
//const char * VideoFaceDetectorApp::defaultSourceVideo = "./data/Marilha_Cam1.wmv";
const char * VideoFaceDetectorApp::defaultSourceVideo = "/media/FILES/LEO/VIDEOS/PrideAndPrejudice/pride_and_prejudice.avi";
const char * VideoFaceDetectorApp::windowName = "video";

int mainVideoFaceDetectorApp(int argc, char * argv[]){
	VideoFaceDetectorApp app = VideoFaceDetectorApp();
	app.run(argc, argv);
	return 0;
}

VideoFaceDetectorApp::VideoFaceDetectorApp()
	: delay(1), backupDelay(1)
{
}

//void VideoFaceDetectorApp::run(int argc, char * argv[]) {
//	if(this->setup(argc,argv)){
//		while(update()){
//			char key = (char)waitKey(delay);
//			if(key == KEY_ESC){
//				break;
//			}
//			else if (key != -1){
//				cout<<"pressed key: " << key << endl;
//				keyPressed(key);
//			}
//		}
//	}
//}

bool VideoFaceDetectorApp::setup(int argc, char * argv[]) {
	if(!detector.setup(defaultCascadeFilename)){
		cerr << "Could not setup face loader with file: " << defaultCascadeFilename << endl;
		return false;
	}
	if(!detector.setupEyesDetector(defaultEyeCascadeFilename)){
		cerr << "Could not setup face loader eye detector with file: " << defaultEyeCascadeFilename << endl;
		return false;
	}

    const char * sourceVideo = (argc < 2 ? defaultSourceVideo : argv[1]);
    namedWindow(windowName);
    if (!player.start(sourceVideo))
    {
        cerr  << "Could not open video " << sourceVideo << endl;
        return false;
    }
    player.enableTrackbar(windowName);

    int videoFrameWidth = (int) player.getVideo().get(CAP_PROP_FRAME_WIDTH);
    int videoFrameHeight = (int) player.getVideo().get(CAP_PROP_FRAME_HEIGHT);

    videoFrameSize = Size(videoFrameWidth,videoFrameHeight);
    drawFaceBoundColor = Scalar(255,0,0);

    delay = 1000/player.getVideoFPS();
    backupDelay = delay;

	return true;
}

bool VideoFaceDetectorApp::update() {
	if(!player.update()){
		return false; //encerra
	}
	else{
		Mat & frame = player.getCurrentFrame();
		//Reduzir tamanho do frame aumenta velocidade, mas diminui precisão
		resize(frame,frame, Size((frame.cols*3)/4,(frame.rows*3)/4));
		if(player.getFrameCount() % 10 == 0){
			this->detector.detectFaces(frame, faces);

			delay = 1;
		}
		else{
			delay = backupDelay;
		}

		drawFaces(frame, faces);

		////////////////////////////////// Show Image /////////////////////////////////////////////
		imshow(windowName, frame);
	}
	return true;
}

void VideoFaceDetectorApp::keyPressed(int key) {
}


//void VideoFaceDetectorApp::detectFaces(const cv::Mat& frame,
//		std::vector<Face>& faces) {
//	vector<Rect> rects;
//
//	Mat processedImage;
//	detector.detectFaces(frame,rects, &processedImage);
//
//	faces.clear();
//	for(size_t i=0; i < rects.size(); ++i){
//		Face face;
//		face.setRegion(rects[i]);
//		faces.push_back(face);
//	}
//	for(size_t i=0; i < faces.size(); ++i){
//		Face & face = faces[i];
//		vector<Rect> eyes;
//		int detect = detector.detectEyes(processedImage, face.getRegion(), eyes);
//
//		cout << "detected " << detect << " eyes " <<endl;
//
//		face.setEyes(eyes);
////		face.getEyes().clear();
////		for(size_t eyeIdx=0; eyeIdx < eyes.size(); ++eyeIdx){
////			face.getEyes().push_back(eyes[i]);
////		}
//	}
//
//}

void VideoFaceDetectorApp::drawFaces(cv::Mat& frame,
		const std::vector<Face>& faces) {
	//drawFaces
	for(size_t i=0; i < faces.size(); ++i){

		const Face & currentFace = faces[i];
		rectangle(frame, currentFace.getRegion(),drawFaceBoundColor);

		for(size_t eyesIdx=0; eyesIdx < currentFace.getEyes().size(); ++eyesIdx){

			Scalar col = Scalar(0,255,0);
			Rect r =  currentFace.getEyes()[eyesIdx];
			rectangle(frame, r, col);
		}
	}
}
