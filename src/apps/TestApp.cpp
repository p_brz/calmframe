/*
 * TestApp.cpp
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#include <TestApp.h>
#include "Poco/File.h"

#include <iostream>
#include <vector>
#include <string>

using namespace std;
using namespace Poco;

vector<string> listFiles(string directory){
	vector<string> files;
	Poco::File file = File(directory);

	if(file.isDirectory()){
		file.list(files);
	}
	return files;
}

bool TestApp::setup(int argc, char* argv[]) {
	vector<string> files = listFiles("./");

	for(size_t i=0; i < files.size(); ++i){
		std::cout << files[i] <<endl;
	}

	return true;
}

bool TestApp::update() {
	return false;
}

void TestApp::keyPressed(int key) {
}
