/*
 * FaceIdentifierApp.h
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#ifndef FACEIDENTIFIERAPP_H_
#define FACEIDENTIFIERAPP_H_

#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include "App.h"
#include "VideoPlayer.h"
#include "ImageLoader.h"

#include "FaceDetector.h"
#include "FaceIdentifier.h"
#include "FacePreprocessor.h"

int mainFaceIdentifier(int argc, char * argv[]);

class FaceIdentifierApp : public App{
private:
//    std::string fn_csv;
//    // These vectors hold the images and corresponding labels.
//    std::vector<cv::Mat> images;
//    std::vector<int> labels;
    ImageLoader imgLoader;

    std::string videoFile;
    VideoPlayer videoPlayer;
	cv::Size videoFrameSize;
    std::string windowName;

    std::string faceCascadeFile;
    std::string eyesCascadeFile;
    std::string eyePairCascadeFile;
    FaceDetector faceDetector;
	std::vector<cv::Rect> faces;
	std::vector<int> faceLabels;
	std::vector<double> faceConfidence;

	cv::Scalar drawFaceBoundColor;

	FaceIdentifier faceIdentifier;
	FacePreprocessor faceAligner;

	int backupDelay;
public:
	bool setup(int argc, char * argv[]);
	bool update();
protected:
	bool setupTraining(const std::string & csvFilename,
			std::vector<cv::Mat> & images, std::vector<int>& labels);
	bool setupIdentifier(int argc, char * argv[]);
	void train(std::vector<cv::Mat> & images, std::vector<int>& labels);
};

#endif /* FACEIDENTIFIERAPP_H_ */
