/*
 * PhotoFrameApp.h
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#ifndef PHOTOFRAMEAPP_H_
#define PHOTOFRAMEAPP_H_

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <vector>

#include "App.h"
#include "LocalPhotoSource.h"
#include "FaceIdentifier.h"
#include "FaceDetector.h"
#include "FacePreprocessor.h"
#include "Face.h"
#include "PersonMonitorObserver.h"
#include "PersonMonitor.h"
#include "VideoPlayer.h"
#include "VideoSource.hpp"
#include "VideoSourceCam.h"

#include "PersonFacesObserver.h"

#include "PhotoDAO.h"
class PhotoFrameApp: public App, PersonFacesObserver, PersonMonitorObserver
{
private:
//	LocalPhotoSource photoSource;
	//TODO: criar classe p/ encapsular configuração/detecção/reconhecimento de faces
	FaceIdentifier faceIdentifier;
	FaceDetector faceDetector;
	FacePreprocessor facePreprocessor;
//	std::vector<Face> faces;
	//TODO: (?) criar abstração p/ representar imagens (reduzir dependências)
	cv::Mat currentPhotoImage;
	Photo currentPhoto;
	std::vector<Photo> photos;
	int currentPhotoIndex;
	PhotoDAO * photoDAO;

	PersonMonitor personMonitor;
//	VideoPlayer capture;
	VideoSource * capture;

    long lastUpdate;
    long minimumTransition;
public:
	bool setup(int argc, char * argv[]);
	bool update();
	void keyPressed(int key);

	//PersonMonitorObserver
	void onPersonsDetected(const std::vector<Person> & persons);
	void onFacesDetected(const std::vector<Face> & faces);
protected:
	void setupPersonMonitor();
	void updatePersonMonitor();
	bool loadPhotos();
	bool train();
	void loadCurrentPhoto();
	void drawFaces();
	void identifyFaces(const cv::_InputArray & faceImg, std::vector<Face> & faces);

	void identifyAllFaces();
	Photo recognizeFaces(const Photo & photo);

	void drawFace(const Face & face, cv::Mat & img) ;


};

#endif /* PHOTOFRAMEAPP_H_ */
