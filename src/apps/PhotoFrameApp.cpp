/*
 * PhotoFrameApp.cpp
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#include <PhotoFrameApp.h>
#include <iostream>
#include "ImageLoader.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "assert.h"
#include <set>
#include <cstdlib>

#include "utils/StringUtils.h"
#include "LocalPhotoDAO.h"

using namespace std;
using namespace cv;

//void debugPhotos(LocalPhotoSource & photoSource){
//	unsigned int currentPhotoIndex = photoSource.getCurrentPhotoNumber();
//
//	cout <<"photos:"<<endl;
//	for(unsigned int i=0; i < photoSource.photoCount(); ++i){
//		photoSource.setCurrentPhoto(i);
//		cout << photoSource.getCurrentPhotoPath() <<endl;
//	}
//
//	photoSource.setCurrentPhoto(currentPhotoIndex);
//}


#define WINDOW_NAME "Photo Frame"
#define FAST_FACE_CASCADE "./data/cascades/lbpcascade_frontalface.xml"
#define RELIABLE_FACE_CASCADE "./data/cascades/haarcascade_frontalface_alt.xml"
#define FACE_CASCADE RELIABLE_FACE_CASCADE
#define EYE_CASCADE "./data/cascades/haarcascade_eye.xml"

bool PhotoFrameApp::setup(int argc, char* argv[]) {
	if(!train()){
		cerr << "Cannot train identifier"<<endl;
		return false;
	}
	if(!loadPhotos()){
		cerr << "Cannot load photos"<<endl;
		return false;
	}

	if(!this->faceDetector.setup(FACE_CASCADE)){
		cerr << "Could not setup face detector from cascade " << FACE_CASCADE << endl;
		return false;
	}

	cout << "Setup face detector with cascade " << FACE_CASCADE << endl;
	assert(faceDetector.isReady());

	if(!this->faceDetector.setupEyesDetector(EYE_CASCADE)){
		cerr << "Could not setup eye face detector from cascade " << EYE_CASCADE << endl;
		return false;
	}

	cout << "Setup eye detector from: " << EYE_CASCADE << endl;
	assert(faceDetector.eyesDetectorIsReady());

    this->identifyAllFaces();

	this->loadCurrentPhoto();

	this->setupPersonMonitor();

//	//Delay para 0, desta forma não atualiza, exceto se usuário pressionar uma tecla
//	delay =0;
	//FIXME: definindo delay p/ valor maior que 0, para permitir atualização do vídeo
	delay =12;

	namedWindow(WINDOW_NAME);

	srand(time(NULL));
//
	lastUpdate = time(NULL);
	minimumTransition = 10;

	return true;
}

bool PhotoFrameApp::loadPhotos() {

//	//FIXME: definir constante com local das fotos
//	photoSource.setRootPath("./images/photos");
//	if(!photoSource.loadPhotos()){
//		cerr << "could not load photos from " << photoSource.getRootPath()<< endl;
//		cout << "loaded photo " << photoSource.getCurrentPhotoPath() << endl;
//		return false;
//	}
//
//	if(this->photoSource.photoCount() <= 0){
//		cerr << "No photo found" << endl;
//		return false;
//	}
	currentPhotoIndex = 0;
	//FIXME: definir constante com local das fotos
//	this->photoDAO = new LocalPhotoDAO("./images/photos");
	this->photoDAO = new LocalPhotoDAO(string("./images/photos"));
	this->photos = this->photoDAO->listAll();

	if(this->photos.size() <= 0){
		cerr << "No photo found" << endl;
		return false;
	}
	return true;
}

void flipAllImages(vector<Mat> & images, vector<int> & labels){
	assert(images.size() == labels.size());
	size_t initialSize = images.size();
	for(size_t i=0; i < initialSize; ++i){
		Mat flipedImage;
		//Produz imagem espelhada
		flip(images[i], flipedImage,1);
		images.push_back(flipedImage);
		labels.push_back(labels[i]);
	}
}

//FIXME: encapsular estes comportamentos/dados em uma classe
//#define TRAIN_IMAGES_FILE "./data/persons_mod.csv"
//#define TRAIN_IMAGES_FILE "./data/persons_processed.csv"
#define TRAIN_IMAGES_FILE "./data/persons_preprocessed.csv"
#define LBPH_RADIUS    3
#define LBPH_NEIGHBORS 8
#define LBPH_GRID_X    8
#define LBPH_GRID_Y    8
//#define LBPH_THRESHOLD 115.0
//#define LBPH_THRESHOLD 145.0
#define LBPH_THRESHOLD 180.0
bool PhotoFrameApp::train() {
	//FIXME: definir constante com local das fotos de treino
	ImageLoader trainImagesLoader = ImageLoader();
	vector<Mat> images;
	vector<int> labels;
	if(!trainImagesLoader.loadImages(TRAIN_IMAGES_FILE,images,labels, IMREAD_GRAYSCALE)){
		cerr << "could not load faces for training from " << TRAIN_IMAGES_FILE<< endl;
		return false;
	}

//	faceIdentifier.setRecognizer(
//			createLBPHFaceRecognizer(LBPH_RADIUS, LBPH_NEIGHBORS, LBPH_GRID_X
//					, LBPH_GRID_Y, LBPH_THRESHOLD));
	faceIdentifier.setRecognizer(
			createEigenFaceRecognizer());

	//Testar
	int sampleIndex = time(NULL) % images.size();
	Mat sampleImage = images[sampleIndex];
	int sampleLabel = labels[sampleIndex];
	images.erase(images.begin() + sampleIndex);
	labels.erase(labels.begin() + sampleIndex);

	//Inverte todas as imagens para aumentar training set e variabilidade dentro dele
	flipAllImages(images,labels);

	faceIdentifier.train(images, labels);

	cout << "Expected label: " << sampleLabel
			<< ". Found label: " << faceIdentifier.identify(sampleImage)
			<< endl;

	return true;
}

bool PhotoFrameApp::update(){
	drawFaces();
	imshow(WINDOW_NAME, this->currentPhotoImage);
	updatePersonMonitor();

	return true;
}

void PhotoFrameApp::drawFaces() {
	const vector<Face> & faces = this->currentPhoto.getFaces();
	for(size_t i=0; i < faces.size(); ++i){
		rectangle(this->currentPhotoImage, faces[i].getRegion(),Scalar(255,0,0),1);
		if(faces[i].getPersonId() >= 0){
			cv::putText(this->currentPhotoImage, StringUtils::toString(faces[i].getPersonId())
						,faces[i].getRegion().tl(), FONT_HERSHEY_SIMPLEX,1, Scalar(255));
		}
	}
}
void PhotoFrameApp::drawFace(const Face & face, Mat & img) {
	rectangle(img, face.getRegion(),Scalar(255,0,0),1);
	if(face.getPersonId() >= 0){
		cv::putText(img, StringUtils::toString(face.getPersonId())
					,face.getRegion().tl(), FONT_HERSHEY_SIMPLEX,1, Scalar(255));
	}
}

#define KEY_RIGHT 83
#define KEY_LEFT 81

void PhotoFrameApp::keyPressed(int key){
//	cout << "Pressed: " << key<<endl;
	if(key == KEY_RIGHT){
//		this->photoSource.nextPhoto();
		this->currentPhotoIndex = (currentPhotoIndex + 1)%this->photos.size();
	}
	else if(key == KEY_LEFT){
//		this->photoSource.previousPhoto();
		this->currentPhotoIndex = (currentPhotoIndex - 1 + this->photos.size())% this->photos.size();
	}
	loadCurrentPhoto();
}

void PhotoFrameApp::loadCurrentPhoto(){
	cout<<"Load Current Photo"<<endl;

//	this->currentPhoto = imread(photoSource.getCurrentPhotoPath());
	currentPhoto = photos[currentPhotoIndex];
	cout << "load photo: " << currentPhoto.getImagePath();
	this->currentPhotoImage = imread(currentPhoto.getImagePath());

	if(currentPhoto.getFacesCount() == Photo::unknownFaceNumber){
		vector<Face> faces;
		this->faceDetector.detectFaces(currentPhotoImage, faces);
		identifyFaces(currentPhotoImage, faces);
		currentPhoto.setFaces(faces);

		this->photos[currentPhotoIndex] = currentPhoto;
		this->photoDAO->update(currentPhoto);
	}

}

void PhotoFrameApp::identifyAllFaces() {
	cout << "IdentifyAllFaces: "<<endl;
	for(size_t i=0; i < this->photos.size(); ++i){
		this->photos[i] = this->recognizeFaces(photos[i]);
		cout<<"."<<endl;
	}
	cout<<endl;
	this->photoDAO->update(this->photos);

	cout << "End face identifying"<<endl;
}

Photo PhotoFrameApp::recognizeFaces(const Photo& photo) {
	if(photo.getFacesCount() == Photo::unknownFaceNumber){

		Mat photoImage = imread(photo.getImagePath());
		vector<Face> faces;
		this->faceDetector.detectFaces(photoImage, faces);
		identifyFaces(photoImage, faces);

		Photo updatedPhoto = photo;
		updatedPhoto.setFaces(faces);

		return updatedPhoto;
	}

	return photo;
}

void PhotoFrameApp::identifyFaces(const cv::_InputArray& faceImg,
		std::vector<Face>& faces)
{
	for(size_t i=0; i < faces.size(); ++i){
		Mat preparedFace;
//		this->facePreprocessor.prepareFace(faceImg.getMat(), preparedFace,faces[i].getRegion());
		preparedFace = this->facePreprocessor.prepareFace(faceImg.getMat(), faces[i]);
		int personId = this->faceIdentifier.identify(preparedFace);
		faces[i].setPersonId(personId);

//		imshow(WINDOW_NAME, preparedFace);
//		waitKey(200);
	}
}

//FIXME: retirar dependência externa
#ifndef VIDEO_SOURCE
//#define VIDEO_SOURCE "/media/FILES/LEO/VIDEOS/PrideAndPrejudice/pride_and_prejudice.avi"
#define VIDEO_SOURCE "/media/FILES/LEO/VIDEOS/PrideAndPrejudice/pride_and_prejudice.avi"
#endif
#ifndef CAM_DEVICE
#define CAM_DEVICE 1
#endif
#define VIDEO_WINDOW "Capture"
void PhotoFrameApp::setupPersonMonitor() {
//	this->capture = new VideoPlayer(VIDEO_SOURCE);
	this->capture = new VideoSourceCam(CAM_DEVICE);
	if(!this->capture->start()){
		cerr << "Could not open video source." <<endl;
//		cerr<<"Could not open cam device " << CAM_DEVICE << ". Trying with test video."<<endl;
//		this->capture = new VideoPlayer(VIDEO_SOURCE);
//		if(!this->capture->start()){
//			cerr<<"Could not open test video: " << VIDEO_SOURCE << ". Check if the video exist"<<endl;
//		}

	}
//	this->personMonitor.setFaceIdentifier(&this->faceIdentifier);
//	this->personMonitor.setPersonCapture(&this->capture.getVideo());
	VideoCapture * capt = &this->capture->getVideo();
	FaceIdentifier * identifierPtr = &this->faceIdentifier;
	FaceDetector * detectorPtr = &this->faceDetector;
	FacePreprocessor * preprocessorPtr = &this->facePreprocessor;
	this->personMonitor = PersonMonitor(capt, identifierPtr
			,detectorPtr, preprocessorPtr);
	this->personMonitor.registerObserver((PersonMonitorObserver *)this);
	this->personMonitor.registerObserver((PersonFacesObserver *)this);

	namedWindow(VIDEO_WINDOW);
	this->capture->enableTrackbar(VIDEO_WINDOW);
}


vector<Photo> filterByPersonId(const std::vector<Photo>& photos, int personId){
	vector<Photo> filteredPhotos;
	if(photos.size() > 0){
		for(size_t i=0; i < photos.size(); ++i){
			set<int> personsId = photos[i].getPersonsId();

			if(personsId.find(personId) != personsId.end()){
				filteredPhotos.push_back(photos[i]);
			}
		}
	}
	return filteredPhotos;
}

void PhotoFrameApp::onPersonsDetected(const std::vector<Person>& persons) {
	cout <<"Detected "<<persons.size() << " persons:";
	for(size_t i=0; i < persons.size(); ++i){
		cout<<" "<<persons[i].getId();
	}
	cout<<endl;

	long currentTime = time(NULL);
	if(persons.size() > 0 && (currentTime - this->lastUpdate) > this->minimumTransition){
		int searchedPerson = persons[rand() % persons.size()].getId();
		set<int> currentPersons = currentPhoto.getPersonsId();
		if(currentPersons.find(searchedPerson) == currentPersons.end()){
			cout <<"Search photos with person: " <<searchedPerson <<endl;

			vector<Photo> photosWithPerson = filterByPersonId(this->photos, searchedPerson);
			cout << "found " <<photosWithPerson.size()<<" photos" <<endl;
			if(photosWithPerson.size() > 0){
				Photo photo = photosWithPerson[rand() % photosWithPerson.size()];
				this->currentPhoto = photo;
				this->currentPhotoImage = imread(photo.getImagePath());

				lastUpdate = time(NULL);
			}
		}
		else{
			cout <<"current Photo already contains person " <<searchedPerson<<endl;
		}
	}
}

void PhotoFrameApp::onFacesDetected(const std::vector<Face>& faces) {

	/*Mat frame = this->capture->getCurrentFrame();
	for(size_t i =0; i < faces.size(); ++i){
		this->drawFace(faces[i],frame);
	}
	imshow(VIDEO_WINDOW,frame);*/
}



void PhotoFrameApp::updatePersonMonitor() {

	if(this->capture->isPlaying() && this->capture->update()){

		imshow(VIDEO_WINDOW,this->capture->getCurrentFrame());
		if(this->capture->getFrameCount() % 10 == 0){
			this->personMonitor.update();
		}
	}
}
