/*
 * TestApp.h
 *
 *  Created on: 25/04/2014
 *      Author: leobrizolara
 */

#ifndef TESTAPP_H_
#define TESTAPP_H_

#include <App.h>

class TestApp: public App {
public:
	bool setup(int argc, char * argv[]);
	bool update();
	void keyPressed(int key);

};

#endif /* TESTAPP_H_ */
