/*
 * playVideo.cpp
 *
 *  Created on: 15/04/2014
 *      Author: leobrizolara
 */

#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O

using namespace std;
using namespace cv;

void trackbarChange(int trackbarPosition, void * data);

int frameNum = -1;          // Frame counter
VideoCapture captVideo;

int playVideoMain(int argc, char *argv[]){
	const char* windowName = "Reference";
//	const char * trackbarName = "videoPositionTrackbar";

    const string sourceVideo = (argc < 2 ? "./data/Marilha_Cam1.wmv" : argv[1]);

    int delay = 30;
    char c;

//    captVideo.open(sourceVideo);
    captVideo.open(1);

    if (!captVideo.isOpened())
    {
        cout  << "Could not open video " << sourceVideo << endl;
        return -1;
    }


//    int videoFrameWidth = (int) captVideo.get(CAP_PROP_FRAME_WIDTH);
//    int videoFrameHeight = (int) captVideo.get(CAP_PROP_FRAME_HEIGHT);
//
//    Size videoFrameSize = Size(videoFrameWidth,videoFrameHeight);
//
//
//    cout << "Try setting frame width to " << videoFrameWidth/2 << " : " <<
//    captVideo.set(CAP_PROP_FRAME_WIDTH, videoFrameWidth/2) << endl;
//    cout << "Try setting frame height to " << videoFrameHeight/2 << " : " <<
//    captVideo.set(CAP_PROP_FRAME_HEIGHT, videoFrameHeight/2) << endl;
//
//    cout <<"FPS: " << captVideo.get(CAP_PROP_FPS) <<endl;
//    int videoSize = (int)captVideo.get(CAP_PROP_FRAME_COUNT);
    // Windows
    namedWindow(windowName, WINDOW_AUTOSIZE);

//    int trackbarPosition = 0;
//    createTrackbar(trackbarName, windowName, &trackbarPosition, videoSize, trackbarChange);

//    cout << "Reference frame resolution: Width=" << videoFrameSize.width << "  Height=" << videoFrameSize.height
//         << " of nr#: " << captVideo.get(CAP_PROP_FRAME_COUNT) << endl;

    Mat frame;
    bool continuePlay = true;
    do //Show the image captured in the window and repeat
    {
    	if(!captVideo.grab()){
            cout << " < < <  Game over!  > > >  (in grab)";
            break;

    	}
        captVideo.retrieve(frame);

        if (frame.empty())
        {
            cout << " < < <  Game over!  > > > ";
            break;
        }

        ++frameNum;
//        setTrackbarPos(trackbarName, windowName, frameNum);

        ////////////////////////////////// Show Image /////////////////////////////////////////////
        imshow(windowName, frame);

        c = (char)waitKey(delay);
        if (c == 27) break;
    }while(continuePlay);

    return 0;
}

void trackbarChange(int trackbarPosition, void * data){
	if(captVideo.isOpened() && trackbarPosition != frameNum){
		cout << "Trackbar changed to " << trackbarPosition << endl;
		captVideo.set(CAP_PROP_POS_FRAMES, trackbarPosition);
		frameNum = trackbarPosition;
	}
}

