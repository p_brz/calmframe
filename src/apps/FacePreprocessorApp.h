
#ifndef FACEPREPROCESSORAPP_H_
#define FACEPREPROCESSORAPP_H_

#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include "FacePreprocessor.h"
#include "FaceDetector.h"
#include "VideoPlayer.h"
#include "ImageLoader.h"
#include "App.h"
#include "Person.h"

#include<map>

class FacePreprocessorApp : public App{
private:
    ImageLoader imgLoader;

    std::string windowName;
    FaceDetector faceDetector;
	std::vector<cv::Rect> faces;
	cv::Scalar drawFaceBoundColor;

	FacePreprocessor facePreprocessor;

	std::vector<cv::Mat> faceImages;
	std::map<int, std::vector<cv::Mat> > imgLabels;
	unsigned int currentImageIndex;
	std::vector<cv::Mat> imagesToSave;
	std::string savePath;
public:
	static const int KEY_ENTER;
	bool setup(int argc, char * argv[]);
	bool update();
	void keyPressed(int key);
protected:
//	void loadPhotoFaces(const std::string & filename, std::map<int,Person> & persons,
//			std::map<int, std::vector<cv::Mat> > & personImages);
	bool setupImages(const std::string & csvFilename,
			std::vector<cv::Mat> & images, std::vector<int>& labels);
	bool setupIdentifier(int argc, char * argv[]);
	void prepareImages(std::vector<cv::Mat> & images, std::vector<int>& labels);
//	void saveImages(const std::vector<cv::Mat> & images, const std::string savePath = ".");
	void selectImages();
	void saveImages(int label, int imgIdx, const cv::Mat & faceImg);
	void saveAllImages(const std::vector<int>& labels);
};

#endif /* FACEIDENTIFIERAPP_H_ */
