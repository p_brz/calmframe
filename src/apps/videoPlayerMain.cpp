/*
 * playVideo.cpp
 *
 *  Created on: 15/04/2014
 *      Author: leobrizolara
 */

#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O

#include "videoPlayerMain.h"
#include "VideoPlayer.h"

using namespace std;
using namespace cv;




int videoPlayerMain(int argc, char *argv[]){
	const char* windowName = "Reference";
    const char * sourceVideo = (argc < 2 ? "./data/Marilha_Cam1.wmv" : argv[1]);
//    VideoPlayer player = VideoPlayer(NULL,windowName,false);
    VideoPlayer player = VideoPlayer();

    namedWindow(windowName);
    if (!player.start(sourceVideo))
    {
        cout  << "Could not open video " << sourceVideo << endl;
        return -1;
    }
    player.enableTrackbar(windowName);

//    int videoFrameWidth = (int) captVideo.get(CAP_PROP_FRAME_WIDTH);
//    int videoFrameHeight = (int) captVideo.get(CAP_PROP_FRAME_HEIGHT);
//
//    Size videoFrameSize = Size(videoFrameWidth,videoFrameHeight);
//
//    cout << "Try setting frame width to " << videoFrameWidth/2 << " : " <<
//    captVideo.set(CAP_PROP_FRAME_WIDTH, videoFrameWidth/2) << endl;
//    cout << "Try setting frame height to " << videoFrameHeight/2 << " : " <<
//    captVideo.set(CAP_PROP_FRAME_HEIGHT, videoFrameHeight/2) << endl;
//
//    cout <<"FPS: " << captVideo.get(CAP_PROP_FPS) <<endl;
//    int videoSize = (int)captVideo.get(CAP_PROP_FRAME_COUNT);
//
//    cout << "Reference frame resolution: Width=" << videoFrameSize.width << "  Height=" << videoFrameSize.height
//         << " of nr#: " << captVideo.get(CAP_PROP_FRAME_COUNT) << endl;

	int frameTimeMiliseconds = 1000/player.getVideoFPS();
//	int frameTimeMiliseconds = 20;
	cout <<"Wait time: "<< frameTimeMiliseconds << endl;
	cout <<"Video FPS: "<< player.getVideoFPS() << endl;
	cout <<"Calculated wait time: "<< 1000/player.getVideoFPS() << endl;

    while(player.update()){
    	imshow(windowName,player.getCurrentFrame());
    	int key = (char)waitKey(frameTimeMiliseconds);
    	if (key == 27 /*ESC*/){
    		break;
    	}
    }

    return 0;
}

