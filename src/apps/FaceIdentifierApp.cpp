/*
 * FaceIdentifierApp.cpp
 *
 *  Created on: 19/04/2014
 *      Author: leobrizolara
 */

#include "FaceIdentifierApp.h"

#include "opencv2/core.hpp"
#include <opencv2/contrib.hpp>
#include "opencv2/highgui.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

#include "utils/StringUtils.h"
#include "FacePreprocessor.h"

bool FaceIdentifierApp::setup(int argc, char * argv[]){

	cout << "Start setup" << endl;
	string csvFilename = (argc > 1 ? argv[1] : "./data/persons_mod.csv");
	vector<Mat> images;
	vector<int> labels;
	cout << "Start Setup Training" << endl;
	if(!this->setupTraining(csvFilename, images, labels)){
		return false;
	}

	cout << "Start Setup identifier" << endl;
    if(!this->setupIdentifier(argc, argv)){ return false;}

	cout << "Start training" << endl;
    this->train(images, labels);

	cout << "End Setup" << endl;

    return true;
}

bool FaceIdentifierApp::setupTraining(const string & csvFilename,
		std::vector<Mat> & images, std::vector<int> & labels)
{

	if(!imgLoader.loadImages(csvFilename,images, labels)){
		cerr << "Error opening file \"" << csvFilename <<  endl;
		// nothing more we can do
		return false;
	}
	// Quit if there are not enough images for this demo.
	if(images.size() <= 1) {
		string error_message = "This demo needs at least 2 images to work. Please add more images to your data set!";
		CV_Error(cv::Error::StsError, error_message);
		return false;
	}

	return true;
}
bool FaceIdentifierApp::setupIdentifier(int argc, char * argv[]){
	faceCascadeFile = "./data/lbpcascade_frontalface.xml";
//	eyesCascadeFile = "./data/haarcascade_eye.xml";
	eyesCascadeFile = "./data/haarcascade_eye_tree_eyeglasses.xml";
	if(!faceDetector.setup(faceCascadeFile)){
		cerr<<"Error initializing face detector with file " << faceCascadeFile << endl;
		return false;
	}
	else{
		cout << "initialized faced detector with file " << faceCascadeFile << endl;
	}

	if(!faceDetector.setupEyesDetector(eyesCascadeFile)){
		cerr<<"Error initializing eyes detector from face detector with files "
				<< eyesCascadeFile << " and "<< eyePairCascadeFile <<  endl;
		return false;
	}

//	videoFile = (argc > 2 ? argv[2] : "./data/Marilha_Cam1.wmv");
	videoFile = (argc > 2 ? argv[2] : "/media/FILES/LEO/VIDEOS/PrideAndPrejudice/pride_and_prejudice.avi");

	if(!videoPlayer.start(videoFile)){
		cerr<<"Error initializing video player with file " << videoFile << endl;
		return false;
	}
	else{
		cout << "initialized video player with file " << videoFile << endl;
	}

	windowName = "window";
    namedWindow(windowName);
    videoPlayer.enableTrackbar(windowName);

	cout << "Created window " << windowName << endl;

    int videoFrameWidth = (int) videoPlayer.getVideo().get(CAP_PROP_FRAME_WIDTH);
    int videoFrameHeight = (int) videoPlayer.getVideo().get(CAP_PROP_FRAME_HEIGHT);

    videoFrameSize = Size(videoFrameWidth,videoFrameHeight);
    drawFaceBoundColor = Scalar(255,0,0);

    delay = 1000/videoPlayer.getVideoFPS();
    backupDelay = delay;

    faceAligner = FacePreprocessor(&faceDetector, Size(200,200));

	return true;
}



#define LBPH_RADIUS    3
#define LBPH_NEIGHBORS 8
#define LBPH_GRID_X    8
#define LBPH_GRID_Y    8
#define LBPH_THRESHOLD 115.0
void FaceIdentifierApp::train(std::vector<cv::Mat> & images, std::vector<int>& labels){
	//    // Get the height from the first image. We'll need this
	//    // later in code to reshape the images to their original
	//    // size:
	//    int height = images[0].rows;
	// The following lines create an LBPH model for
	// face recognition and train it with the images and
	// labels read from the given CSV file.
	//
	// The LBPHFaceRecognizer uses Extended Local Binary Patterns
	// (it's probably configurable with other operators at a later
	// point), and has the following default values
	//
	//      radius = 1
	//      neighbors = 8
	//      grid_x = 8
	//      grid_y = 8
	//
	// So if you want a LBPH FaceRecognizer using a radius of
	// 2 and 16 neighbors, call the factory method with:
	//
	//      cv::createLBPHFaceRecognizer(2, 16);
	//
	// And if you want a threshold (e.g. 123.0) call it with its default values:
	//
	//      cv::createLBPHFaceRecognizer(1,8,8,8,123.0)
	//

	this->faceIdentifier.setRecognizer(
			createLBPHFaceRecognizer(LBPH_RADIUS,LBPH_NEIGHBORS, LBPH_GRID_X, LBPH_GRID_Y, LBPH_THRESHOLD));
//	this->faceIdentifier.setRecognizer(createFisherFaceRecognizer());
	cout << "Identifier train" << endl;
	this->faceIdentifier.train(images, labels);
}


bool FaceIdentifierApp::update() {
	if(!videoPlayer.update()){
		return false; //encerra
	}
	else{
		Mat & frame = videoPlayer.getCurrentFrame();
		//Reduzir tamanho do frame aumenta velocidade, mas diminui precisão
		resize(frame,frame, Size((frame.cols*3)/4,(frame.rows*3)/4));

		if(videoPlayer.getFrameCount() % 5 == 0){
			//Detect faces
			delay = 1;
			faces.clear();
			faceLabels.clear();
			faceConfidence.clear();
			Mat processedImage;

			faceDetector.detectFaces(frame,faces, &processedImage);
			for(size_t i=0; i < faces.size(); ++i){
				Mat imageFace;
				faceAligner.prepareFace(processedImage, imageFace, faces[i],false);

				double confidence = 0.0;
				int label = this->faceIdentifier.identify(imageFace, &confidence);

				cout << "Identified Label " << label << " with "
						<< confidence << "% of confidence" << endl;

				faceLabels.push_back(confidence > 0.85 ? label : -1);
				faceConfidence.push_back(confidence);
//				labels.push_back(-1);
			}
		}
		else{
			delay = backupDelay;
		}
		//drawFaces
		for(size_t i=0; i < faces.size(); ++i){
			rectangle(frame, faces[i],drawFaceBoundColor);


			if(faceLabels.size() > i && faceLabels[i] != -1){
				string labelText = StringUtils::toString(faceLabels[i])
									+ "; " + StringUtils::toString(faceConfidence[i]);
				putText(frame, labelText,faces[i].tl()
						,FONT_HERSHEY_PLAIN, 1,Scalar(255));
			}
		}

		////////////////////////////////// Show Image /////////////////////////////////////////////
		imshow(windowName, frame);
	}
	return true;
}

int mainFaceIdentifier(int argc, char * argv[]){
	FaceIdentifierApp faceIdApp;

	return faceIdApp.run(argc, argv);
}
