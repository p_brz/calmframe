
#include "detectFaceOnVideo.h"

#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion

#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O

using namespace std;
using namespace cv;

#include "FaceDetector.h"

void keyPressed(int key){

}

int detectFaceOnVideoMain(int argc, char *argv[]){

//    stringstream conv;

	FaceDetector faceDetector = FaceDetector();

	//lbp cascade é mais rápida, porém menos precisa
//	const char * cascadeFilename = "./data/haarcascade_frontalface_alt.xml";
	const char * cascadeFilename = "./data/lbpcascade_frontalface.xml";
	if(!faceDetector.setup(cascadeFilename)){
		cerr << "Could not setup face loader with file: " << cascadeFilename << endl;
		exit(1);
	}

    const string sourceVideo = "./data/Marilha_Cam1.wmv";
//    int psnrTriggerValue, delay;
//    conv << argv[3] << endl << argv[4];       // put in the strings
//    conv >> psnrTriggerValue >> delay;        // take out the numbers

    int delay = 30;

    int frameNum = -1;          // Frame counter

    VideoCapture captVideo(sourceVideo);

    if (!captVideo.isOpened())
    {
        cout  << "Could not open video " << sourceVideo << endl;
        return -1;
    }


    int videoFrameWidth = (int) captVideo.get(CAP_PROP_FRAME_WIDTH);
    int videoFrameHeight = (int) captVideo.get(CAP_PROP_FRAME_HEIGHT);

    Size videoFrameSize = Size(videoFrameWidth,videoFrameHeight);


    cout << "Setting frame width to " << videoFrameWidth/2 << " : " <<
    captVideo.set(CAP_PROP_FRAME_WIDTH, videoFrameWidth/2) << endl;
    cout << "Setting frame height to " << videoFrameHeight/2 << " : " <<
    captVideo.set(CAP_PROP_FRAME_HEIGHT, videoFrameHeight/2) << endl;

    cout <<"FPS: " << captVideo.get(CAP_PROP_FPS) <<endl;

    const char* windowName = "Reference";

    // Windows
    namedWindow(windowName, WINDOW_AUTOSIZE);

    cout << "Reference frame resolution: Width=" << videoFrameSize.width << "  Height=" << videoFrameSize.height
         << " of nr#: " << captVideo.get(CAP_PROP_FRAME_COUNT) << endl;

    vector<Rect> faces;
	Scalar drawColor = Scalar(255,0,0);
	int backupDelay = delay;
    Mat frame;

    for(;;) //Show the image captured in the window and repeat
    {
        captVideo >> frame;

        if (frame.empty())
        {
            cout << " < < <  Game over!  > > > ";
            break;
        }

        ++frameNum;
//        cout << "Frame: " << frameNum << "# "<< endl;

        //Reduzir tamanho do frame aumenta velocidade, mas diminui precisão
        resize(frame,frame, Size((frame.cols*3)/4,(frame.rows*3)/4));
        if(frameNum % 10 == 0){
			//Detect faces
			faces.clear();
			faceDetector.detectFaces(frame,faces);
			delay = 1;
        }
        else{
        	delay = backupDelay;
        }
        //drawFaces
        for(size_t i=0; i < faces.size(); ++i){
			rectangle(frame, faces[i],drawColor);
        }

        ////////////////////////////////// Show Image /////////////////////////////////////////////
        imshow(windowName, frame);

        int key = waitKey(delay);
        if(key == 27 /*ESC*/){
        	break;
        }
        else if(key != -1){
        	keyPressed(key);
        }
    }

    return 0;
}


