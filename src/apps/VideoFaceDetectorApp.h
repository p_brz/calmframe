/*
 * VideoFaceDetectorApp.h
 *
 *  Created on: 16/04/2014
 *      Author: leobrizolara
 */

#ifndef VIDEOFACEDETECTORAPP_H_
#define VIDEOFACEDETECTORAPP_H_

#include <opencv2/highgui.hpp>
#include <vector>
#include <map>
#include "VideoPlayer.h"
#include "FaceDetector.h"
#include "Face.h"
#include "App.h"

int mainVideoFaceDetectorApp(int argc, char * argv[]);

class VideoFaceDetectorApp : public App{
public:
	static const char * defaultCascadeFilename;
    static const char * defaultEyeCascadeFilename;
    static const char * defaultSourceVideo;
    static const char * windowName;

	int delay;
protected:
	static const int KEY_ESC;

	VideoPlayer player;
	cv::Size videoFrameSize;

	FaceDetector detector;
	std::vector<Face> faces;
//	std::vector<cv::Rect> faces;
//	std::map<int,std::vector<cv::Rect> > detectedEyes;
	cv::Scalar drawFaceBoundColor;

	int backupDelay;
public:
	VideoFaceDetectorApp();
//	void run(int argc, char * argv[]);
	bool setup(int argc, char * argv[]);
	bool update();
	void keyPressed(int key);

//	void detectFaces(const cv::Mat & frame, std::vector<Face> & faces);
	void drawFaces(cv::Mat & image, const std::vector<Face> &faces);
};

#endif /* VIDEOFACEDETECTORAPP_H_ */
