#include "FacePreprocessorApp.h"

#include "opencv2/core.hpp"
#include "opencv2/contrib.hpp"
#include "opencv2/highgui.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include <assert.h>

#include "Poco/File.h"
#include "Poco/Path.h"

using namespace cv;
using namespace std;
using namespace Poco;

#include "utils/StringUtils.h"
#include "utils/CsvReader.h"
#include "utils/CsvRow.h"
#include "Person.h"
#include "ImagePreprocessor.h"

const int FacePreprocessorApp::KEY_ENTER = 10;

#define FACE_CASCADE "./data/lbpcascade_frontalface.xml"
#define EYE_CASCADE "./data/haarcascade_eye.xml"
//	eyesCascadeFile = "./data/haarcascade_eye.xml";
//	eyesCascadeFile = "./data/haarcascade_eye_tree_eyeglasses.xml";

bool FacePreprocessorApp::setup(int argc, char * argv[]){

	facePreprocessor = FacePreprocessor();

	cout << "Start setup" << endl;
	string csvFilename = (argc > 1 ? argv[1] : "./data/persons_raw.csv");
	vector<Mat> images;
	vector<int> labels;
	cout << "Start Setup Training" << endl;
	if(!this->setupImages(csvFilename, images, labels)){
		return false;
	}

    if(!this->setupIdentifier(argc, argv)){ return false;}

    this->prepareImages(images, labels);

    this->selectImages();

//    this->saveAllImages(labels);

	cout << "End Setup" << endl;
	this->currentImageIndex = 0;

    return true;
}

bool FacePreprocessorApp::setupImages(const string & csvFilename,
		std::vector<Mat> & images, std::vector<int> & labels)
{

	if(!imgLoader.loadImages(csvFilename,images, labels)){
		cerr << "Error opening file \"" << csvFilename <<  endl;
		// nothing more we can do
		return false;
	}
	// Quit if there are not enough images for this demo.
	if(images.size() <= 1) {
		string error_message = "This demo needs at least 2 images to work. Please add more images to your data set!";
		CV_Error(cv::Error::StsError, error_message);
		return false;
	}

	this->savePath = "./data/persons_preprocessed";

	return true;
}
bool FacePreprocessorApp::setupIdentifier(int argc, char * argv[]){
	cout << "Start Setup identifier" << endl;

	if(!(faceDetector.setup(FACE_CASCADE) && faceDetector.setupEyesDetector(EYE_CASCADE))){
		cerr<<"Error initializing face detector with cascades "
				<< FACE_CASCADE << " and "<< EYE_CASCADE<< endl;
		return false;
	}

	windowName = "window";
    namedWindow(windowName);

    delay=0;

	return true;
}

void FacePreprocessorApp::prepareImages(std::vector<cv::Mat> & images, std::vector<int>& labels){
	cout << "Start images preparation" << endl;

	vector<Face> tmpFaces;
	for(unsigned int i=0; i < images.size(); ++i){
		//TODO: definir tamanho fixo (ou tamanho máximo) para imagem
		Mat grayImg;
		//TODO: melhorar etapa de preprocessamento
		ImagePreprocessor::instance()->convertToGray(images[i], grayImg);
		ImagePreprocessor::instance()->equalize(grayImg, grayImg);

		faceDetector.detectFaces(grayImg,tmpFaces);
		cout << "#" << i<<": "<<tmpFaces.size() << " faces found..." << endl;

		for(unsigned int faceIdx=0; faceIdx < tmpFaces.size(); ++faceIdx){
			if(tmpFaces[faceIdx].hasLeftEye() && tmpFaces[faceIdx].hasRightEye()){
				Mat preparedFace = this->facePreprocessor.prepareFace(grayImg,tmpFaces[faceIdx]);

				assert(!preparedFace.empty());

				this->imgLabels[labels[i]].push_back(preparedFace);
				faceImages.push_back(preparedFace);
			}
			else{
				cerr << "\tdon't found eyes"<<endl;
			}
		}

		tmpFaces.clear();
	}

	cout << "finish images preparation" << endl;


}


void FacePreprocessorApp::saveImages(int label, int imgIdx, const cv::Mat& faceImg) {
	const string labelStr = StringUtils::toString(label);
	const string imgIndexStr =  StringUtils::toString(imgIdx);

	Path dirpath = Path(savePath,"person" + labelStr);
	//Cria diretórios se não existem
	File dirfile(dirpath);
	dirfile.createDirectories();
	//Definir path da imagem e salvar
	string filename = labelStr + "_" + imgIndexStr  + ".jpeg";
	Path imgFilepath = Path(dirpath, filename);

	cout << "Saving " << imgFilepath.toString() << endl;

	imwrite(imgFilepath.toString(), faceImg);

}

void FacePreprocessorApp::saveAllImages(const std::vector<int>& labels) {
	cout << "save all images" << endl;

	for(unsigned int labelIdx=0; labelIdx < labels.size(); ++labelIdx){
		for(unsigned int imgIdx=0; imgIdx < this->imgLabels[labelIdx].size(); ++imgIdx){
			string filename = StringUtils::toString(labelIdx) + "_"
					+ StringUtils::toString(imgIdx)  + ".jpeg";
			string imgFilepath = savePath + "/" + filename;

			cout << "Saving " << imgFilepath << endl;

			imwrite(imgFilepath, this->imgLabels[labelIdx][imgIdx]);
		}
	}
}


void FacePreprocessorApp::selectImages() {
	typedef std::map<int, std::vector<cv::Mat> > MapIntToMat;
	for(MapIntToMat::iterator it=this->imgLabels.begin();
			it != imgLabels.end(); ++it)
	{
		const vector<Mat> faceImages = it->second;
		for(size_t faceIdx = 0, savedFaceCount=0; faceIdx < faceImages.size(); ++faceIdx){
			imshow(windowName, faceImages[faceIdx]);
			char key = (char)waitKey(0);
			if(key != ' '){
				saveImages(it->first, savedFaceCount, faceImages[faceIdx]);
				++savedFaceCount;
			}
			else{
				cout << "pass"<<endl;
			}
		}
	}


}

bool FacePreprocessorApp::update() {
	return false;
}

void FacePreprocessorApp::keyPressed(int key){

}
