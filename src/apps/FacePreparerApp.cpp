#include "FacePreparerApp.h"

#include "opencv2/core.hpp"
#include "opencv2/contrib.hpp"
#include "opencv2/highgui.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

#include "utils/StringUtils.h"


const int FacePreparerApp::KEY_ENTER = 10;

bool FacePreparerApp::setup(int argc, char * argv[]){

	facePreprocessor = FacePreprocessor(&this->faceDetector,Size(200,200));

	cout << "Start setup" << endl;
	string csvFilename = (argc > 1 ? argv[1] : "./data/persons.csv");
	vector<Mat> images;
	vector<int> labels;
	cout << "Start Setup Training" << endl;
	if(!this->setupImages(csvFilename, images, labels)){
		return false;
	}

	cout << "Start Setup identifier" << endl;
    if(!this->setupIdentifier(argc, argv)){ return false;}

	cout << "Start training" << endl;
    this->prepareImages(images, labels);

    cout << "save all images" << endl;
    this->saveAllImages(labels);

	cout << "End Setup" << endl;
	this->currentImageIndex = 0;

    return true;
}

bool FacePreparerApp::setupImages(const string & csvFilename,
		std::vector<Mat> & images, std::vector<int> & labels)
{

	if(!imgLoader.loadImages(csvFilename,images, labels)){
		cerr << "Error opening file \"" << csvFilename <<  endl;
		// nothing more we can do
		return false;
	}
	// Quit if there are not enough images for this demo.
	if(images.size() <= 1) {
		string error_message = "This demo needs at least 2 images to work. Please add more images to your data set!";
		CV_Error(cv::Error::StsError, error_message);
		return false;
	}

	this->savePath = "./data/persons";

	return true;
}
bool FacePreparerApp::setupIdentifier(int argc, char * argv[]){
	faceCascadeFile = "./data/lbpcascade_frontalface.xml";
//	eyesCascadeFile = "./data/haarcascade_eye.xml";
	eyesCascadeFile = "./data/haarcascade_eye_tree_eyeglasses.xml";
	if(!faceDetector.setup(faceCascadeFile)){
		cerr<<"Error initializing face detector with file " << faceCascadeFile << endl;
		return false;
	}
	else{
		cout << "initialized faced detector with file " << faceCascadeFile << endl;
	}

	if(!faceDetector.setupEyesDetector(eyesCascadeFile)){
//	if(!faceDetector.setupEyesDetector(eyesCascadeFile)){
		cerr<<"Error initializing eyes detector from face detector with files "
				<< eyesCascadeFile << " and "<< eyePairCascadeFile <<  endl;
		return false;
	}

	windowName = "window";
    namedWindow(windowName);

    delay=0;

	return true;
}

void FacePreparerApp::prepareImages(std::vector<cv::Mat> & images, std::vector<int>& labels){
	vector<Mat> tmpFacesImg;
	for(unsigned int i=0; i < images.size(); ++i){
		facePreprocessor.prepareFaces(images[i], tmpFacesImg,true);


		cout << "#" << i<<": "<<tmpFacesImg.size() << " faces found..." << endl;

		this->imgLabels[labels[i]].insert(
				imgLabels[labels[i]].end(),tmpFacesImg.begin(), tmpFacesImg.end());

		faceImages.insert(faceImages.end(),tmpFacesImg.begin(), tmpFacesImg.end());

		tmpFacesImg.clear();
	}
}


void FacePreparerApp::saveAllImages(const std::vector<int>& labels) {
	for(unsigned int labelIdx=0; labelIdx < labels.size(); ++labelIdx){
		for(unsigned int imgIdx=0; imgIdx < this->imgLabels[labelIdx].size(); ++imgIdx){
			string filename = StringUtils::toString(labelIdx) + "_"
					+ StringUtils::toString(imgIdx)  + ".jpeg";
			string imgFilepath = savePath + "/" + filename;

			cout << "Saving " << imgFilepath << endl;

			imwrite(imgFilepath, this->imgLabels[labelIdx][imgIdx]);
		}
	}
}

bool FacePreparerApp::update() {
	if(currentImageIndex >= this->faceImages.size()){
		return false;
	}

	imshow(windowName, faceImages[currentImageIndex]);

	return true;
}

void FacePreparerApp::keyPressed(int key) {
	++currentImageIndex;

//	cout<<"key: " << (char)key << " int: "<< key<<endl;
}

//void FacePreparerApp::saveImages(const std::vector<cv::Mat>& images,
//		const std::string savePath) {
//	for(unsigned int i=0; i < images.size(); ++i){
//		string imgFilename = savePath + "/" + StringUtils::toString(i) + ".jpeg";
//
//		cout << "Saving " << imgFilename << endl;
//
//		imwrite(savePath + "/" + StringUtils::toString(i) + ".jpeg", images[i]);
//	}
//}

