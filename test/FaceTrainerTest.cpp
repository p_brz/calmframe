
#include "gtest/gtest.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <map>

using namespace std;
using namespace cv;

#include "FaceTrainer.h"
#include "utils/CsvReader.h"
#include "utils/StringUtils.h"
#include "Person.h"
#include "Face.h"
#include "ImageLoader.h"
#include "FaceDetector.h"

class FaceTrainerTest : public testing::Test {
protected:
	FaceTrainer faceTrainer;
	FaceDetector faceDetector;

	string personImagesCsv;
	string faceCascade;
	string eyesCascade;
public:
	FaceTrainerTest(){
		personImagesCsv = "./test/data/persons_train.csv";
		faceCascade = "./data/cascades/haarcascade_frontalface_alt.xml";
		eyesCascade = "./data/cascades/haarcascade_eye.xml";
		faceDetector.setup(faceCascade);
		faceDetector.setupEyesDetector(eyesCascade);
	}

	void setUp(){
		ASSERT_TRUE(faceDetector.isReady());
		ASSERT_TRUE(faceDetector.eyesDetectorIsReady());
		faceTrainer = FaceTrainer();
	}

	void givenSomePersonImagesAndLabels(vector<Mat> & personImages, vector<int> & labels)
	{
		ImageLoader imgLoader;
		imgLoader.loadImages(personImagesCsv,personImages,labels);
	}
	vector<Face> givenSomeFacesFromImages(const vector<Mat> & personImages)
	{
		vector<Face> faces;
		for(size_t i=0; i < personImages.size(); ++i){

			vector<Face> currentFaces;
			int detectedFaces = faceDetector.detectFaces(personImages[i],currentFaces);
			if(detectedFaces > 0){
				faces.push_back(currentFaces[0]);
			}
			if(detectedFaces != 1){
				cerr<<"Expected 1 face per image. Error in image " << i
						<< " found " << detectedFaces << " faces" << endl;
			}

		}
		return faces;
	}

	void whenAddingAllFaces(vector<Mat> & personImages, vector<Face> & faces, vector<int> & labels){

	}

	void shouldStartWithNoFaces(){
		shouldHaveNTrainFaces(0);
	}
	void shouldHaveNTrainFaces(unsigned int N){
		vector<Mat> dummyMats;
		vector<int> dummyInts;
		unsigned int facesToTrain = faceTrainer.getFacesToTrain(dummyMats, dummyInts);
		ASSERT_EQ(N, facesToTrain);
	}
};

TEST_F(FaceTrainerTest, trainSomeFacesCenario){
	shouldStartWithNoFaces();
//	vector<Mat> personImages;
//	vector<int> labels;
//	givenSomePersonImagesAndLabels(personImages,labels);
//	vector<Face> faces = givenSomeFacesFromImages(personImages);
//	EXPECT_EQ(personImages.size(), faces.size());
//	whenAddingAllFaces(personImages,faces,labels);
//	shouldHaveNTrainFaces(personImages.size());
}
