/*
 * EyeDetectionTest.cpp
 *
 *  Created on: 02/05/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"

#include "FaceDetector.h"
#include "EyeDetector.h"
#include "ImagePreprocessor.h"
#include "ImageLoader.h"
#include "LocalPhotoSource.h"
#include <opencv2/core.hpp>

#include <iostream>

using namespace std;
using namespace cv;

class EyeDetectionTest : public testing::Test {
 protected:
	FaceDetector detector;
	EyeDetector eyeDetector;
	vector<Mat> images;
	LocalPhotoSource photoSource;
	static const char * facesFilename;
	static const char * faceCascade;
	static const char * eyeCascade;
	static const char * leftEyeCascade;
	static const char * rightEyeCascade;
public:
	EyeDetectionTest(){
//		ImageLoader loader;
//		vector<int> labels;
//		loader.loadImages(facesFilename,images,labels);
		photoSource.setRootPath("./images/photos");
		photoSource.loadPhotos();

		for(unsigned int i=0; i < photoSource.photoCount(); ++i){
			string imgPath = photoSource.getCurrentPhotoPath();
			images.push_back(imread(imgPath));
			photoSource.nextPhoto();
		}

		detector.setup(faceCascade);
		eyeDetector.setup(eyeCascade);
	}
	void setUp(){

	}
};

const char * EyeDetectionTest::facesFilename = "./test/testFiles/testFaces.csv";
//const char * EyeDetectionTest::faceCascade = "./data/lbpcascade_frontalface.xml";
const char * EyeDetectionTest::faceCascade = "./data/haarcascade_frontalface_alt.xml";
const char * EyeDetectionTest::eyeCascade = "./data/haarcascade_eye.xml";
const char * EyeDetectionTest::leftEyeCascade = "./data/haarcascade_mcs_lefteye.xml";
const char * EyeDetectionTest::rightEyeCascade = "./data/haarcascade_mcs_righteye.xml";
//const char * EyeDetectionTest::eyeCascade = "./data/haarcascade_eye_tree_eyeglasses.xml";

TEST_F(EyeDetectionTest, testEyeDetection){
	string winName = "window";

//	for(size_t imgIndex = 0; imgIndex < images.size(); ++imgIndex){
//		Mat imgFace = images[imgIndex];
//
//
//		ImagePreprocessor::preprocessImage(imgFace,imgFace);
//		vector<Rect> faces;
//		detector.detectFaces(imgFace, faces);
//
//
//		namedWindow(winName);
//
//		for(size_t i=0; i < faces.size(); ++i){
//			rectangle(imgFace, faces[i], Scalar(255,0,0),1);
//
//			Rect left, right;
//			eyeDetector.detectEyes(imgFace,faces[i], left,right);
//
//			rectangle(imgFace, left, Scalar(255,0,0),1);
//			rectangle(imgFace, right, Scalar(0,255,0),1);
//
//
//			Rect lRegion, rRegion;
//			eyeDetector.extractEyesRegion(faces[i], lRegion, rRegion);
//			rectangle(imgFace, lRegion, Scalar(122),1);
//			rectangle(imgFace, rRegion, Scalar(122),1);
//
//		}
//
//		imshow(winName, imgFace);
//		waitKey(0);
//	}
}
