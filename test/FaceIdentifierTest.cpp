#include "gtest/gtest.h"
#include <iostream>
#include <cstdlib>
#include <opencv2/contrib.hpp>
#include <map>
using namespace std;
using namespace cv;

#include "FaceIdentifier.h"
#include "ImageLoader.h"

#include "utils/Searcher.h"
#include "ImageComparator.h"

#define TRAIN_IMAGES_CSV "./test/data/persons_train.csv"
#define TEST_IMAGES_CSV "./test/data/persons_test.csv"
#define PREPROCESSED_FACES_CSV "./data/persons_processed.csv"

class FaceIdentifierTest : public testing::Test {
 protected:
	FaceIdentifier faceIdentifier;

	vector<Mat> faces;
	vector<int> labels;

	vector<Mat> trainImages;
	vector<int> trainLabels;

	vector<Mat> testImages;
	vector<int> testLabels;

	vector<Mat> unkownFaces;
	vector<int> unkownLabels;

	ImageComparator comparator;
public:
//	FaceIdentifierTest(){
//		ImageLoader loader;
//		loader.loadImages(TEST_IMAGES_CSV,testImages, testLabels,IMREAD_GRAYSCALE);
//		loader.loadImages(TEST_IMAGES_CSV,testImages, testLabels,IMREAD_GRAYSCALE);
//
//		//FIXME: preparar imagens de treino
//		for(size_t i=0; i < trainImages.size(); ++i){
//			resize(trainImages[i],trainImages[i],Size(70,70));
//		}
//		//FIXME: preparar imagens de test
//		for(size_t i=0; i < testImages.size(); ++i){
//			resize(testImages[i],testImages[i],Size(70,70));
//		}
//
//		this->selectUnknownFaces();
//	}
	FaceIdentifierTest(){
		ImageLoader loader;
		loader.loadImages(PREPROCESSED_FACES_CSV,faces, labels,IMREAD_GRAYSCALE);


		//FIXME: redefinir tamanho das imagens
		for(size_t i=0; i < faces.size(); ++i){
			resize(faces[i],faces[i],Size(70,70));
		}

		this->selectFaces();
	}


	void selectFaces(){
		this->selectUnknownFaces();
		this->splitTrainAndTestFaces();
	}


//	void selectUnknownFaces(){
//		srand(time(NULL));
//		int randLabelIndex = rand() % trainLabels.size();
//		int unkownLabel = trainLabels[randLabelIndex];
//
//		for(size_t i=0; i < trainLabels.size(); ++i){
//			if(trainLabels[i] == unkownLabel){
//				unkownLabels.push_back(trainLabels[i]);
//				unkownFaces.push_back(trainImages[i]);
//			}
//		}
//		excludePersonWithLabel(trainLabels[randLabelIndex], trainImages,trainLabels);
//		excludePersonWithLabel(trainLabels[randLabelIndex], testImages,testLabels);
//	}
	void selectUnknownFaces(){
		srand(time(NULL));
		int randLabelIndex = rand() % faces.size();
		int unkownLabel = labels[randLabelIndex];

		for(size_t i=0; i < labels.size(); ++i){
			if(labels[i] == unkownLabel){
				unkownLabels.push_back(labels[i]);
				unkownFaces.push_back(faces[i]);
			}
		}
		excludePersonWithLabel(labels[randLabelIndex], faces,labels);
	}

#define MIN_TRAIN_IMAGES_NUM 5
#define TEST_IMAGES_NUM 3

	void splitTrainAndTestFaces(){
		map<int,vector<int> > labelsToImagesIndex;

		for(size_t i=0; i < labels.size(); ++i){
			labelsToImagesIndex[labels[i]].push_back(i);
		}

		//Selecionar imagens
		for(map<int,vector<int> >::iterator
				 it = labelsToImagesIndex.begin()
				;it != labelsToImagesIndex.end(); ++it)
		{
			vector<int> & imgsIndex = it->second;

			//Selecionar imagens para treino
			size_t countTrainImages = imgsIndex.size();
			if( imgsIndex.size() > MIN_TRAIN_IMAGES_NUM + TEST_IMAGES_NUM ){
				countTrainImages = (imgsIndex.size() - TEST_IMAGES_NUM );
			}
			else if(imgsIndex.size() > MIN_TRAIN_IMAGES_NUM){
				countTrainImages = MIN_TRAIN_IMAGES_NUM;
			}

			size_t index = 0;
			while(index < countTrainImages){
				int faceIdx = imgsIndex[index];
				trainImages.push_back(this->faces[faceIdx]);
				trainLabels.push_back(this->labels[faceIdx]);

				++index;
			}

			//Selecionar o resto das imagens p/ teste
			while(index < imgsIndex.size()){
				int faceIdx = imgsIndex[index];
				testImages.push_back(this->faces[faceIdx]);
				testLabels.push_back(this->labels[faceIdx]);

				++index;
			}
		}
	}

	void setUp(){
		faceIdentifier = FaceIdentifier();
	}

	void givenFaceIdentifierWithEigenFaceRecognizer(){
		Ptr<FaceRecognizer> recognizer = createEigenFaceRecognizer();
		faceIdentifier.setRecognizer(recognizer);
	}
	void givenFaceIdentifierWithFisherFaceRecognizer(){
		Ptr<FaceRecognizer> recognizer = createFisherFaceRecognizer();
		faceIdentifier.setRecognizer(recognizer);
	}

	void givenThatIdentifierIsTrained(){
		faceIdentifier.train(trainImages,trainLabels);
	}

	void excludePersonWithLabel(int label, vector<Mat> & images, vector<int> & labels){
		vector<int> excludeLabels;
		excludeLabels.push_back(label);
		this->excludePersonWithLabels(excludeLabels,images,labels);
	}

	void excludePersonWithLabels(const vector<int> & labelsToExclude, vector<Mat> & images, vector<int> & labels){
		vector<Mat> finalImages;
		vector<int> finalLabels;
		for(size_t i=0; i < labels.size(); ++i){
			if(!Searcher::instance()->existElement(labelsToExclude,labels[i])){
				finalLabels.push_back(labels[i]);
				finalImages.push_back(images[i]);
			}
		}

		images = finalImages;
		labels = finalLabels;
		for(size_t i=0; i < labelsToExclude.size(); ++i){
			ASSERT_FALSE(Searcher::instance()->existElement(labels,labelsToExclude[i]));
		}
	}

	Mat givenAUnknownFace(){
//		bool isEmpty = this->unkownFaces.empty();
//		ASSERT_FALSE(isEmpty);
		int randFaceIndex = rand() % this->unkownFaces.size();

		return this->unkownFaces[randFaceIndex];
	}
	Mat givenAKnownTrainedFace(){
//		bool isEmpty = this->unkownFaces.empty();
//		ASSERT_FALSE(isEmpty);
		int randFaceIndex = rand() % this->trainImages.size();

		return this->trainImages[randFaceIndex];
	}
	Mat givenAKnownTestFace(){
//		bool isEmpty = this->unkownFaces.empty();
//		ASSERT_FALSE(isEmpty);
		int randFaceIndex = rand() % this->testImages.size();

		return this->testImages[randFaceIndex];
	}

	void recognizerShouldHaveEigenvectorsAndMeanParameters(const FaceRecognizer * recognizer){
		vector<String> params;
		recognizer->getParams(params);

		EXPECT_GE(Searcher::instance()->findElement(params, String("eigenvectors")), 0);
		EXPECT_GE(Searcher::instance()->findElement(params, String("mean")), 0);
	}

	void printSimilarity(const Mat & faceImg){
		Mat reconstructedFace = this->faceIdentifier.reconstructFace(faceImg);
		double similarity = comparator.getSimilarity(faceImg,reconstructedFace);
		cout << "similarity: " << similarity << endl;
	}
};


TEST_F(FaceIdentifierTest, eigenfaceRecognizerShouldHaveEigenvectorsAndMeanParameters){
	givenFaceIdentifierWithEigenFaceRecognizer();

	recognizerShouldHaveEigenvectorsAndMeanParameters(faceIdentifier.getRecognizer());
}
TEST_F(FaceIdentifierTest, fisherfaceRecognizerShouldHaveEigenvectorsAndMeanParameters){
	givenFaceIdentifierWithFisherFaceRecognizer();

	recognizerShouldHaveEigenvectorsAndMeanParameters(faceIdentifier.getRecognizer());
}

#include <opencv2/highgui.hpp>

TEST_F(FaceIdentifierTest, identifierShouldNotRecognizeUnknownFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();
	Mat unknownFace = givenAUnknownFace();
	EXPECT_FALSE(faceIdentifier.isAKnownFace(unknownFace));
	Mat reconstructedFace = faceIdentifier.reconstructFace(unknownFace);

	namedWindow("unknownFace");
	namedWindow("reconstructedFace");
	imshow("unknownFace", unknownFace);
	imshow("reconstructedFace", reconstructedFace);
	waitKey(0);
	destroyAllWindows();
}


TEST_F(FaceIdentifierTest, identifierShouldRecognizeAKnownTrainedFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();
	Mat knownFace = givenAKnownTrainedFace();
	EXPECT_TRUE(faceIdentifier.isAKnownFace(knownFace));
	Mat reconstructedFace = faceIdentifier.reconstructFace(knownFace);

	string knowFaceWin = "knownFace";
	string reconstructedFaceWin = "reconstructedFace";
	namedWindow(knowFaceWin);
	namedWindow(reconstructedFaceWin);
	imshow(knowFaceWin, knownFace);
	imshow(reconstructedFaceWin, reconstructedFace);
	waitKey(0);
	destroyAllWindows();
}

TEST_F(FaceIdentifierTest, identifierShouldRecognizeAKnownTestFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();
	Mat knownTestFace = givenAKnownTestFace();
	EXPECT_TRUE(faceIdentifier.isAKnownFace(knownTestFace));
	Mat reconstructedFace = faceIdentifier.reconstructFace(knownTestFace);

	string knowFaceWin = "knownTestFace";
	string reconstructedFaceWin = "reconstructedFace";
	namedWindow(knowFaceWin);
	namedWindow(reconstructedFaceWin);
	imshow(knowFaceWin, knownTestFace);
	imshow(reconstructedFaceWin, reconstructedFace);
	waitKey(0);
	destroyAllWindows();
}



TEST_F(FaceIdentifierTest, identifierShouldNotRecognizeNoneUnknownFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();

	for(size_t i =0; i < this->unkownFaces.size(); ++i){
		printSimilarity(this->unkownFaces[i]);
//		EXPECT_FALSE(faceIdentifier.isAKnownFace(this->unkownFaces[i]));
	}
}
TEST_F(FaceIdentifierTest, identifierShouldRecognizeAllKnownTrainedFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();

	for(size_t i =0; i < this->unkownFaces.size(); ++i){
		printSimilarity(this->trainImages[i]);
//		EXPECT_TRUE(faceIdentifier.isAKnownFace(this->trainImages[i]));
	}
}
TEST_F(FaceIdentifierTest, identifierShouldRecognizeAllKnownTestFace){
	givenFaceIdentifierWithEigenFaceRecognizer();
	givenThatIdentifierIsTrained();

	for(size_t i =0; i < this->unkownFaces.size(); ++i){
		printSimilarity(this->testImages[i]);
//		EXPECT_TRUE(faceIdentifier.isAKnownFace(this->testImages[i]));
	}
}

