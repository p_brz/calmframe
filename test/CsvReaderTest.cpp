/*
 * CsvReaderTest.cpp
 *
 *  Created on: 29/04/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include <iostream>

#include "utils/CsvReader.h"
using namespace std;

// To use a test fixture, derive a class from testing::Test.
class CsvReaderTest : public testing::Test {
 protected:
	CsvReader csvReader;
public:
	void tearDown(){
		csvReader.close();
	}
};

#define TEST_FILE "./test/testFiles/test.csv"

TEST_F(CsvReaderTest, defaultConstructor) {
	EXPECT_FALSE(csvReader.isOpen());
	EXPECT_TRUE(csvReader.getCurrentRow().isEmpty());
	EXPECT_LT(csvReader.getRowIndex(), 0);
}
TEST_F(CsvReaderTest, openFile) {
	csvReader.open(TEST_FILE);
	EXPECT_LT(csvReader.getRowIndex(), 0);

	int i =0;
	while(csvReader.nextRow()){
		EXPECT_EQ(i,csvReader.getRowIndex());
		EXPECT_FALSE(csvReader.getCurrentRow().isEmpty());
		EXPECT_GT(csvReader.getCurrentRow().getColumnCount(), 0);
		++i;
	}
	EXPECT_EQ(10, csvReader.getRowIndex());
}

