#include "gtest/gtest.h"

#include "FaceDetector.h"
#include "EyeDetector.h"
#include "ImagePreprocessor.h"
#include "ImageLoader.h"
#include "LocalPhotoSource.h"
#include <opencv2/core.hpp>

#include "FacePreprocessor.h"
#include "utils/GeomUtils.h"
#include <iostream>

using namespace std;
using namespace cv;

class FacePreprocessorTest : public testing::Test {
 protected:
	FaceDetector detector;
	EyeDetector eyeDetector;
	vector<Mat> images;
	LocalPhotoSource photoSource;
	static const char * facesFilename;
	static const char * faceCascade;
	static const char * eyeCascade;
	static const char * leftEyeCascade;
	static const char * rightEyeCascade;

	FacePreprocessor preprocessor;
public:
	FacePreprocessorTest(){
//		ImageLoader loader;
//		vector<int> labels;
//		loader.loadImages(facesFilename,images,labels);
		photoSource.setRootPath("./images/photos");
		photoSource.loadPhotos();

		for(unsigned int i=0; i < photoSource.photoCount(); ++i){
			string imgPath = photoSource.getCurrentPhotoPath();
			images.push_back(imread(imgPath));
			photoSource.nextPhoto();
		}

		detector.setup(faceCascade);
		eyeDetector.setup(eyeCascade);
	}

	void loadSomeImageAndFaces(Mat & image, vector<Face> & faces){
		vector<Rect> faceRegions;
		loadSomeImageAndFaces(image, faceRegions);
		for(size_t i=0; i < faceRegions.size(); ++i){
			Face face = Face(faceRegions[i]);

			Rect left, right;
			eyeDetector.detectEyes(image,faceRegions[i], left,right);

			face.setLeftEye(left);
			face.setRightEye(right);
			faces.push_back(face);
		}
	}
	void loadSomeImageAndFaces(Mat & image, vector<Rect> & faceRegions){
		Mat imgWithFaces;
		bool foundImage = false;

		size_t imgIndex = 0;
		while(imgIndex < images.size() && !foundImage)
		{
			faceRegions.clear();
			imgWithFaces= images[imgIndex];
			ImagePreprocessor::preprocessImage(imgWithFaces,imgWithFaces);
			foundImage = (detector.detectFaces(imgWithFaces, faceRegions) > 0);

			++imgIndex;
		}

		ASSERT_EQ(true, foundImage);
		ASSERT_GT(faceRegions.size(), (size_t)0);
		image = imgWithFaces;
	}

	void setUp(){

	}
};

const char * FacePreprocessorTest::facesFilename = "./test/testFiles/testFaces.csv";
//const char * EyeDetectionTest::faceCascade = "./data/lbpcascade_frontalface.xml";
const char * FacePreprocessorTest::faceCascade = "./data/haarcascade_frontalface_alt.xml";
const char * FacePreprocessorTest::eyeCascade = "./data/haarcascade_eye.xml";
const char * FacePreprocessorTest::leftEyeCascade = "./data/haarcascade_mcs_lefteye.xml";
const char * FacePreprocessorTest::rightEyeCascade = "./data/haarcascade_mcs_righteye.xml";
//const char * EyeDetectionTest::eyeCascade = "./data/haarcascade_eye_tree_eyeglasses.xml";

#ifdef SLOW_TESTS
TEST_F(FacePreprocessorTest, defaultFaceAlignShouldNotChangeImageSize){
	Mat imgFace;
	vector<Face> faces;

	loadSomeImageAndFaces(imgFace,faces);

	for(size_t i=0; i < faces.size(); ++i){
		Rect left = faces[i].getLeftEye();
		Rect right = faces[i].getRightEye();

		if(left.area() > 0 && right.area() > 0){
			Point lEyeCenter = GeomUtils::center<int>(left);
			Point rEyeCenter = GeomUtils::center<int>(right);

			Mat transformedFace = preprocessor.alignFace(imgFace, faces[i].getRegion(), lEyeCenter,rEyeCenter);

			EXPECT_EQ(faces[i].getRegion().size().width, transformedFace.cols);
			EXPECT_EQ(faces[i].getRegion().size().height, transformedFace.rows);
		}
	}
}
#endif

#ifdef SLOW_TESTS
TEST_F(FacePreprocessorTest, showFaceAlign)
{
#ifdef SHOWTEST
	string winName = "showFaceAligner";
	namedWindow(winName);
#endif
	Mat imgFace;
	vector<Face> faces;
	loadSomeImageAndFaces(imgFace,faces);

	EXPECT_GT(faces.size(), (size_t)0);

	for(size_t i=0; i < faces.size(); ++i){
		Rect left = faces[i].getLeftEye();
		Rect right = faces[i].getRightEye();

		rectangle(imgFace, left, Scalar(255,0,0),1);
		rectangle(imgFace, right, Scalar(0,255,0),1);

		if(left.area() > 0 && right.area() > 0){
			Point lEyeCenter = GeomUtils::center<int>(left);
			Point rEyeCenter = GeomUtils::center<int>(right);

			//Desenha circulo no centro do olho
			circle(imgFace,lEyeCenter, 3,Scalar(255),-1);
			circle(imgFace,rEyeCenter, 3,Scalar(255),-1);

			Mat transformedFace = preprocessor.alignFace(imgFace, faces[i].getRegion(), lEyeCenter,rEyeCenter,
					preprocessor.getSizeFace());

			EXPECT_EQ(preprocessor.getSizeFace().width, transformedFace.cols);
			EXPECT_EQ(preprocessor.getSizeFace().height, transformedFace.rows);

#ifdef SHOWTEST
			imshow(winName, transformedFace);
			waitKey(0);
#endif
		}
	}

#ifdef SHOWTEST
	imshow(winName, imgFace);
	waitKey(0);
#endif
}
#endif

bool isEquals(const Mat & img0,const Mat & img1){

	bool isEquals = (img0.cols == img1.cols && img0.rows == img1.rows);

	if(isEquals){
		for(int i = 0; i < img0.rows; i++)
		{
			const uchar* originIndex = img0.ptr<uchar>(i);
			const uchar* imgFaceIndex = img1.ptr<uchar>(i);
			for(int j = 0; j < img0.cols; j++){
				if(originIndex[j] != imgFaceIndex[j]){
					return false;
				}
			}
		}
	}

	return isEquals;
}

#define IMAGE_FACE_PATH "./test/data/images/faceTransformed.jpeg"
TEST_F(FacePreprocessorTest, faceEqualizationShouldNotChangeOriginalData){
	Mat imgFace = imread(IMAGE_FACE_PATH);
	ImagePreprocessor::preprocessImage(imgFace,imgFace);


	Mat originImage = imgFace.clone();
	this->preprocessor.equalizeFace(imgFace);

	ASSERT_EQ(originImage.cols, imgFace.cols);
	ASSERT_EQ(originImage.rows, imgFace.rows);

	ASSERT_TRUE(isEquals(originImage,imgFace));
}

#define IMAGE_EQUALIZED_PATH "./test/data/images/faceEqualized.jpeg"

TEST_F(FacePreprocessorTest, showFaceEqualization){
	const Mat imgFace = imread(IMAGE_FACE_PATH);
	const Mat equalizedFace = this->preprocessor.equalizeFace(imgFace);
	EXPECT_EQ(1,equalizedFace.channels());
	const Mat smoothedImg = this->preprocessor.smoothFace(equalizedFace);
	Mat maskedImg = smoothedImg.clone();
	this->preprocessor.applyMaskOn(maskedImg);

#ifdef SHOWTEST
	string imgFaceWinName = "imgFace";
	namedWindow(imgFaceWinName);
	string equalizedWinName = "equalizedFace";
	namedWindow(equalizedWinName);
	string smoothWinName = "smoothedImageFace";
	namedWindow(smoothWinName);
	string maskedWinName = "maskedFace";
	namedWindow(maskedWinName);

	imshow(imgFaceWinName, imgFace);
	imshow(equalizedWinName, equalizedFace);
	imshow(smoothWinName, smoothedImg);
	imshow(maskedWinName, maskedImg);

	waitKey(0);
#endif
}

//TEST_F(FacePreprocessorTest, showPrepareFace)
//{
//	string winName = "showPrepareFace";
//	namedWindow(winName);
//	Mat imgFace;
//	vector<Face> faces;
//	loadSomeImageAndFaces(imgFace,faces);
//
//	EXPECT_GT(faces.size(), (size_t)0);
//
//	for(size_t i=0; i < faces.size(); ++i){
//		Rect left = faces[i].getLeftEye();
//		Rect right = faces[i].getRightEye();
//
////		rectangle(imgFace, left, Scalar(255,0,0),1);
////		rectangle(imgFace, right, Scalar(0,255,0),1);
//
//		if(left.area() > 0 && right.area() > 0){
//			Point lEyeCenter = GeomUtils::center<int>(left);
//			Point rEyeCenter = GeomUtils::center<int>(right);
//
////			//Desenha circulo no centro do olho
////			circle(imgFace,lEyeCenter, 3,Scalar(255),-1);
////			circle(imgFace,rEyeCenter, 3,Scalar(255),-1);
//
//			Rect faceRegion = faces[i].getRegion();
//			Mat preparedFace =
//					preprocessor.prepareFace(imgFace,faceRegion,lEyeCenter,rEyeCenter);
//			EXPECT_EQ(preprocessor.getSizeFace().width,preparedFace.cols);
//			EXPECT_EQ(preprocessor.getSizeFace().height,preparedFace.rows);
//
//			imshow(winName, preparedFace);
//			waitKey(0);
//
//		}
//	}
//}

//#define IMAGE_SMOOTH_PATH "./test/images/faceSmoothing.jpeg"
//TEST_F(FacePreprocessorTest, showFaceSmoothing){
////	Mat imgFace = imread(IMAGE_FACE_PATH);
//	Mat imgFace = imread(IMAGE_EQUALIZED_PATH);
//	const Mat expectedFace = imread(IMAGE_SMOOTH_PATH);
//
//	Mat equalizedFace = this->preprocessor.equalizeFace(imgFace);
//	Mat smoothedImg = this->preprocessor.smoothFace(equalizedFace);
//
//	string imgFaceWinName = "imgFace";
//	namedWindow(imgFaceWinName);
//	string smoothWinName = "smoothedImageFace";
//	namedWindow(smoothWinName);
//	string expectedWinName = "expectedEqualizedFace";
//	namedWindow(expectedWinName);
//
//	imshow(imgFaceWinName, imgFace);
//	imshow(smoothWinName, smoothedImg);
//	imshow(expectedWinName, expectedFace);
//
//	waitKey(0);
//}

