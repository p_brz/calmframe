
#include "gtest/gtest.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/imgproc.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;
using namespace cv;

#include "utils/CsvReader.h"
#include "utils/StringUtils.h"
#include "Person.h"
#include "Face.h"
#include "ImageLoader.h"
#include "FaceDetector.h"

class FaceDetectorTest : public testing::Test {
protected:
	FaceDetector faceDetector;

	string personImagesCsv;
	string faceCascade;
	string eyesCascade;
public:
	FaceDetectorTest(){
		personImagesCsv = "./test/data/persons_train.csv";
		faceCascade = "./data/cascades/haarcascade_frontalface_alt.xml";
		eyesCascade = "./data/cascades/haarcascade_eye.xml";
	}

	void setUp(){
		faceDetector = FaceDetector();
	}

	vector<Mat> givenSomePersonImages(){
		vector<Mat> personImages;
		vector<int> dummyLabels;

		ImageLoader loader;
		loader.loadImages(personImagesCsv,personImages, dummyLabels);

		return personImages;
	}
};


Mat limitImgSize(const Mat & img, const Size & maxSize){
	if(!img.empty()){
		int deltaH = img.rows - maxSize.height;
		int deltaW = img.cols - maxSize.width;
		if(deltaH > 0 || deltaW > 0){
			float scale = 1;
			if(deltaH > deltaW){
				scale = maxSize.height/(float)img.rows;
			}
			else{
				scale = maxSize.width/(float)img.cols;
			}
			Mat outImg;
			resize(img,outImg,Size(img.cols*scale, img.rows*scale));
			return outImg;
		}
	}
	return img;
}

#ifdef SLOW_TESTS

#define MAX_PERSON_IMAGES 10u
#define MAX_WIDTH 640
#define MAX_HEIGHT 480
TEST_F(FaceDetectorTest, testDetectionErrorRate){
	faceDetector.setup(faceCascade);
	ASSERT_TRUE(faceDetector.isReady());
	vector<Mat> personImages = givenSomePersonImages();

	ASSERT_GT(personImages.size(), 0u);

	int falsePositiveCount=0, falseNegativeCount=0;

	unsigned int personImagesSize
			= (personImages.size() <= MAX_PERSON_IMAGES ? personImages.size() : MAX_PERSON_IMAGES);

	for(unsigned int i=0; i < personImagesSize; ++i){
		Mat img = limitImgSize(personImages[i], Size(MAX_WIDTH, MAX_HEIGHT));
		ASSERT_LE(img.cols,MAX_WIDTH);
		ASSERT_LE(img.rows,MAX_HEIGHT);
		ASSERT_GT(img.cols,0);
		ASSERT_GT(img.rows,0);

		vector<Face> dummyFaces;
		int detectedFaces = faceDetector.detectFaces(personImages[i], dummyFaces);
		if(detectedFaces > 1){
			++falsePositiveCount;
		}
		else if(detectedFaces == 0){
			++falseNegativeCount;
		}
	}

	cout << "False Positive rate: " << falsePositiveCount/(float)personImagesSize << endl;
	cout << "False Negative rate: " << falseNegativeCount/(float)personImagesSize << endl;

}

#endif
