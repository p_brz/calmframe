#include "gtest/gtest.h"

#include <opencv2/core.hpp>
#include <iostream>

using namespace std;
using namespace cv;

#include "LocalPhotoSource.h"
#include "LocalPhotoDAO.h"

#include "utils/StringUtils.h"

#define IMAGES_PATH "./images/photos"
//#define IMAGES_PATH "./test/data/images/persons/test"

class PhotoDAOTest : public testing::Test {
 protected:
	LocalPhotoSource photoSource;
	PhotoDAO * photoDAO;
public:
	PhotoDAOTest(){
		photoDAO = NULL;
		photoSource.setRootPath(IMAGES_PATH);
		photoSource.loadPhotos(true);
		EXPECT_GT(photoSource.photoCount(),0u);
	}
	void setUp(){
		photoDAO = NULL;
	}
	void tearDown(){
		if(photoDAO != NULL){
			delete photoDAO;
		}
	}

	void shouldListFilesFromPhotoSource(PhotoDAO * photoDAO){
		vector<Photo> photos = photoDAO->listAll();

		ASSERT_EQ(photoSource.photoCount(), photos.size());
		for(size_t i=0; i<photos.size(); ++i){
			EXPECT_EQ(photoSource.getPhotoPath(i), photos[i].getImagePath());
		}
	}
	void shouldListZeroFilesAfterRemoveAll(PhotoDAO * photoDao){
		photoDao->removeAll();
		ASSERT_EQ(0u,photoDao->listAll().size());
	}
	void shouldAddOnePhotoAfterCreate(PhotoDAO * photoDao){
		unsigned int size = photoDao->listAll().size();
		Photo somePhoto = Photo("fileThatNotExist");
		photoDao->create(somePhoto);
		ASSERT_EQ(size + 1,photoDao->listAll().size());
	}
	void shouldAddNPhotosAfterCreateMany(PhotoDAO * photoDao){
		vector<Photo> manyPhotos;
		for(size_t i=0; i < 10; ++i){
			manyPhotos.push_back(Photo(StringUtils::toString(i)));
		}

		unsigned int initialSize = photoDao->listAll().size();
		photoDao->create(manyPhotos);
		ASSERT_EQ(initialSize + manyPhotos.size(),photoDao->listAll().size());
	}
};

TEST_F(PhotoDAOTest, shouldListFilesFromPhotoSource){
	shouldListFilesFromPhotoSource(new LocalPhotoDAO(this->photoSource));
}
TEST_F(PhotoDAOTest, shouldListZeroFilesAfterRemoveAll){
	photoDAO = new LocalPhotoDAO(this->photoSource);
	shouldListZeroFilesAfterRemoveAll(photoDAO);
}
TEST_F(PhotoDAOTest, shouldAddOnePhotoAfterCreate){
	photoDAO = new LocalPhotoDAO(this->photoSource);
	shouldAddOnePhotoAfterCreate(photoDAO);
}
TEST_F(PhotoDAOTest, shouldAddNPhotosAfterCreateMany){
	photoDAO = new LocalPhotoDAO(this->photoSource);
	shouldAddNPhotosAfterCreateMany(photoDAO);
}
