/*
 * CsvRowTest.cpp
 *
 *  Created on: 29/04/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include <iostream>

#include "utils/CsvRow.h"
using namespace std;

// To use a test fixture, derive a class from testing::Test.
class CsvRowTest : public testing::Test {
 protected:
	CsvRow csvRow;
public:
	void setUp(){
		csvRow = CsvRow();
	}
};

#define EXAMPLE_ROW "./data/persons_mod/s33/33_4.jpeg;0"

TEST_F(CsvRowTest, defaultConstructor) {
	EXPECT_TRUE(csvRow.isEmpty());
	EXPECT_EQ(0, csvRow.getColumnCount());
}
TEST_F(CsvRowTest, stringConstructor) {
	csvRow = CsvRow(EXAMPLE_ROW,';');
	EXPECT_FALSE(csvRow.isEmpty());
	EXPECT_EQ(2, csvRow.getColumnCount());
	EXPECT_EQ(string("./data/persons_mod/s33/33_4.jpeg"), csvRow.get(0));
	EXPECT_EQ(string("0"), csvRow.get(1));
}
TEST_F(CsvRowTest, emptyStringConstructor) {
	csvRow = CsvRow("",';');
	EXPECT_TRUE(csvRow.isEmpty());
	EXPECT_EQ(0, csvRow.getColumnCount());
}

TEST_F(CsvRowTest, vectorConstructor) {
	string valuesArray[] = {"some","value"};
	vector<string> values;
	values.push_back(valuesArray[0]);
	values.push_back(valuesArray[1]);

	csvRow = CsvRow(values);

	values[0] = "";

	EXPECT_EQ(2, csvRow.getColumnCount());
	EXPECT_EQ(valuesArray[0], csvRow.get(0));
	EXPECT_EQ(valuesArray[1], csvRow.get(1));
}

