#include "gtest/gtest.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <map>

using namespace std;
using namespace cv;

#include "PersonRecognizer.h"
#include "utils/CsvReader.h"
#include "utils/StringUtils.h"
#include "Person.h"
#include "Face.h"


void loadPhotoFaces(const string & filename,
		map<int, Person> & persons,
		map<int, vector<string> > & personImages)
{
	CsvReader reader;
	reader.open(filename);

	Mat faceImage;
	vector<Face> faces;

	CsvRow currentRow;
	while(reader.nextRow(&currentRow)){
		if(currentRow.getColumnCount() >= 2){
			string imgPath = currentRow.get(0);
			int personId = StringUtils::toNumber<int>(currentRow.get(1));

			if(persons.find(personId) == persons.end()){
				Person somePerson(personId);
				persons[personId] = somePerson;
			}
			personImages[personId].push_back(imgPath);
		}
	}
}

class PersonRecognizerTest : public testing::Test {
protected:
	PersonRecognizer personRecognizer;
	map<int,Person> persons;
	map<int, vector<string> > personImages;
public:
	PersonRecognizerTest(){
		srand(time(NULL));
		loadPhotoFaces("./test/data/persons_test.csv", persons,personImages);
	}


	void setUp(){
		personRecognizer = PersonRecognizer();
	}

	void recognizerShouldStartNotReady(){
		ASSERT_FALSE(personRecognizer.isReady());
	}
	void givenThatRecognizerIsReady(){
		personRecognizer.setup();
		EXPECT_TRUE(personRecognizer.isReady());
	}
	Mat givenSomePersonImage(){
		if(personImages.size() > 0){
			string imgPath;
			//Assume que todos os ids estejam entre 0 e personImages.size -1
			int somePersonId = rand() % personImages.size();
			vector<string> imgPaths = personImages[somePersonId];
			string someImgPath = imgPaths[rand() % imgPaths.size()];
			return imread(someImgPath);
		}
		return Mat();
	}
	void recognizerShouldDetectFaces(Mat personImage){
		vector<Face> faces = personRecognizer.detectFaces(personImage);
		EXPECT_GT(faces.size(), 0u);
	}
};

TEST_F(PersonRecognizerTest, detectingFacesOnImage){
	recognizerShouldStartNotReady();
	givenThatRecognizerIsReady();
	Mat personImage = givenSomePersonImage();
	EXPECT_FALSE(personImage.empty());
	recognizerShouldDetectFaces(personImage);
}
