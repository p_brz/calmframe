#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "Poco/Path.h"

using namespace std;
using namespace cv;
using namespace Poco;

#include "ImageComparator.h"

#define IMAGE_TEST_FOLDER "./test/data/images"

class ImageComparatorTest : public testing::Test {
 protected:
	ImageComparator comparator;
	Path imgTestFolder;
public:
	ImageComparatorTest(){
		imgTestFolder = IMAGE_TEST_FOLDER;
	}
	void setUp(){
		comparator = ImageComparator();
	}

	Mat givenSomeImage(){
		Mat img = imread(Path(imgTestFolder,"personX_0.jpeg").toString() );
		return img;
	}
	Mat givenLargeImage(){
		Mat largeImage = imread(Path(imgTestFolder,"large.jpg").toString());
		assert(!largeImage.empty());
		return largeImage;
	}
	Mat givenSomeEmptyImage(){
		return Mat();
	}
	void givenTwoDifferentImages(Mat & img1,Mat & img2){
		img1 = imread(Path(imgTestFolder,"personX_0.jpeg").toString());
//		img2 = imread(Path(imgTestFolder,"personY_1.jpeg").toString());
		Mat lrgImg = givenLargeImage();
		resize(lrgImg,img2,Size(img1.cols,img1.rows));
	}
	void givenTwoSimilarImages(Mat & img1,Mat & img2){
		img1 = imread(Path(imgTestFolder,"personX_0.jpeg").toString());
		img2 = imread(Path(imgTestFolder,"personX_1.jpeg").toString());
	}
	void givenTwoImagesWithDifferentSizes(Mat & img1,Mat & img2){
		img1 = givenSomeImage();
		img2 = givenLargeImage();

		assert(img1.cols*img1.rows != img2.cols*img2.rows);
	}
};

TEST_F(ImageComparatorTest, should_give_total_similarity_when_compare_the_same_image){
	Mat img = givenSomeImage();
	EXPECT_EQ(1.0, comparator.getSimilarity(img,img));
}

TEST_F(ImageComparatorTest, minimum_similarity_should_be_0){
	Mat whiteImage, blackImage;
	whiteImage = Mat(Size(64,64),CV_8U,Scalar(255));
	blackImage = Mat(Size(64,64),CV_8U,Scalar(0));

	EXPECT_EQ(0,comparator.getSimilarity(whiteImage,blackImage));
}
TEST_F(ImageComparatorTest, should_give_low_similarity_when_compare_different_images){
	Mat img1, img2;
	givenTwoDifferentImages(img1,img2);

	double similarity = comparator.getSimilarity(img1,img2);

	cout << "similarity was: " << similarity << endl;

	EXPECT_LT(similarity, 0.5);
	EXPECT_GE(similarity, 0);
}

#include <opencv2/highgui.hpp>

TEST_F(ImageComparatorTest, should_give_high_similarity_when_compare_similar_images){
	Mat img1, img2;
	givenTwoSimilarImages(img1,img2);
	double similarity = comparator.getSimilarity(img1,img2);

	cout << "similarity was: " << similarity << endl;

	EXPECT_GT(similarity, 0.5);
}
TEST_F(ImageComparatorTest, should_give_negative_similarity_when_compare_with_a_empty_images){
	Mat img = givenSomeImage();
	Mat emptyImage = givenSomeEmptyImage();

	EXPECT_LT(comparator.getSimilarity(img,emptyImage),0);
}
TEST_F(ImageComparatorTest, should_give_negative_similarity_when_compare_images_with_different_sizes){
	Mat img1, img2;
	givenTwoImagesWithDifferentSizes(img1,img2);

	EXPECT_LT(comparator.getSimilarity(img1,img2),0);
}
