#include "gtest/gtest.h"

#include "FaceDetector.h"
#include "EyeDetector.h"
#include "ImagePreprocessor.h"
#include "ImageLoader.h"
#include "LocalPhotoSource.h"
#include <opencv2/core.hpp>

#include "utils/GeomUtils.h"
#include <iostream>

using namespace std;
using namespace cv;

class GeomUtilsTest : public testing::Test {
 protected:
//	FaceDetector detector;
//	EyeDetector eyeDetector;
	vector<Mat> images;
	LocalPhotoSource photoSource;

	GeomUtils geom;
public:
	GeomUtilsTest(){
//		ImageLoader loader;
//		vector<int> labels;
//		loader.loadImages(facesFilename,images,labels);
		photoSource.setRootPath("./images/photos");
		photoSource.loadPhotos();

		for(unsigned int i=0; i < photoSource.photoCount(); ++i){
			string imgPath = photoSource.getCurrentPhotoPath();
			images.push_back(imread(imgPath));
			photoSource.nextPhoto();
		}
	}
	void setUp(){
		geom = GeomUtils();
	}
};

TEST_F(GeomUtilsTest, testDistance){
	Point2f origin(0,0), pt0(4,3), pt1(0,5), pt2(0,-5), pt3(10,0), pt4(-2,0);
	EXPECT_EQ(5, geom.distance(origin,pt0));
	EXPECT_EQ(5, geom.distance(origin,pt1));
	EXPECT_EQ(5, geom.distance(origin,pt2));
	EXPECT_EQ(10, geom.distance(origin,pt3));
	EXPECT_EQ(2, geom.distance(origin,pt4));

	EXPECT_EQ(10, geom.distance(pt1,pt2));
	EXPECT_EQ(12, geom.distance(pt3,pt4));
}
